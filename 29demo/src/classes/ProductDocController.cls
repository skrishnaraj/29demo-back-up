public with sharing class ProductDocController {
  List<String> repDV,opDV,repDV1,opDV1,repDV2,opDV2=new List<String>();
  
  Boolean editMode=false;
  
 public Product__c prod {get;private set;}
    public ProductDocController(ApexPages.StandardController controller) {
       this.prod= (Product__c)controller.getRecord(); 
//       system.assertNotEquals(this.prod.id,null);
      // string Conid=Apexpages.Currentpage().getParameters().get('id');
      if(this.prod != null){
       getDetailsofProduct(this.prod.id);
       editMode=true;
       }
    }
    public void getDetailsofProduct(string pid)
    {
    if(pid != null && pid != '')
    {
        String RD1,OD1,RD2,OD2,RD3,OD3;
      Product__c prod = [Select Required_Product_Doc__c,Required_Doc_Identity_proof__c,Required_Doc_Income_Proof__c,Optional_Product_doc__c,Optional_Doc_Identity_Proof__c,Optional_Doc_Income_Proof__c From Product__c where id=:pid];

      if(prod.Required_Product_Doc__c!= null)
      repDV=prod.Required_Product_Doc__c.split(';');
      if(prod.Optional_Product_doc__c!= null)
      opDV=prod.Optional_Product_doc__c.split(';');
      if(prod.Required_Doc_Identity_proof__c!= null)
      repDV1=prod.Required_Doc_Identity_proof__c.split(';');
      if(prod.Optional_Doc_Identity_Proof__c!= null)
      opDV1=prod.Optional_Doc_Identity_Proof__c.split(';');
      if(prod.Required_Doc_Income_Proof__c!= null)
      repDV2=prod.Required_Doc_Income_Proof__c.split(';');
      if(prod.Optional_Doc_Income_Proof__c!= null)
      opDV2=prod.Optional_Doc_Income_Proof__c.split(';');
      
    }
    
    
    
    }
    
    
    
    String[] selectedItems1 =new String[]{};

public String selectedProduct{ get; set;}
    public String[] getSelectedItems1() {
        return selectedItems1;
    }

    public void setSelectedItems1(String[] selectedItems1) {
        this.selectedItems1 = selectedItems1;
    }
    
    

public String productDocument { get; set
    {
    productDocument=value;
    } 
}

public list<SelectOption> getItems4(){
    List<SelectOption> options = new List<SelectOption>();
    if(opDV != null){
    for(Integer i = 0; i < opDV.size(); i++){
    options.add(new SelectOption(opDV[i],opDV[i]));
    }
    }
    return options;
    }
public list<SelectOption> getItems5(){
    List<SelectOption> options = new List<SelectOption>();
    if(repDV != null){
    for(Integer i = 0; i < repDV.size(); i++){
    options.add(new SelectOption(repDV[i],repDV[i]));
    }
    }
    return options;
    }
    // for second
    public list<SelectOption> getItems6(){
    List<SelectOption> options = new List<SelectOption>();
    if(opDV1 != null){
    for(Integer i = 0; i < opDV1.size(); i++){
    options.add(new SelectOption(opDV1[i],opDV1[i]));
    }
    }
    return options;
    }
public list<SelectOption> getItems7(){
    List<SelectOption> options = new List<SelectOption>();
    if(repDV1 != null){
    for(Integer i = 0; i < repDV1.size(); i++){
    options.add(new SelectOption(repDV1[i],repDV1[i]));
    }
    }
    return options;
    }
    // for third
    public list<SelectOption> getItems8(){
    List<SelectOption> options = new List<SelectOption>();
    if(opDV2 != null){
    for(Integer i = 0; i < opDV2.size(); i++){
    options.add(new SelectOption(opDV2[i],opDV2[i]));
    }
    }
    return options;
    }
public list<SelectOption> getItems9(){
    List<SelectOption> options = new List<SelectOption>();
    if(repDV2 != null){
    for(Integer i = 0; i < repDV2.size(); i++){
    options.add(new SelectOption(repDV2[i],repDV2[i]));
    }
    }
    return options;
    }
    // complete
    
public list<SelectOption> getItems1(){
    List<SelectOption> options = new List<SelectOption>();
  // List<ProductDoc__c> productDocument= new List<ProductDoc__c>();
    //productDocument = [Select id,TProduct_1__c From ProductDoc__c];
    Map<String, ProductDoc__c> custSetting= ProductDoc__c.getAll();
    
    //Map<String, ProductDoc__c> instance = new Map<String, ProductDoc__c>();
    for(ProductDoc__c instance : custSetting.values()){
    if(instance.Type__c == 'Address Proof'){
    options.add(new SelectOption(instance.Name,instance.Name));
    }
    }
    
    
   /* for(ProductDoc__c p:productDocument){
        options.add(new SelectOption('0',p.TProduct_1__c));
    } */
    return options;
}

public list<SelectOption> getItems2(){
    List<SelectOption> options = new List<SelectOption>();
    Map<String, ProductDoc__c> custSetting= ProductDoc__c.getAll();
    for(ProductDoc__c instance : custSetting.values()){
    if(instance.Type__c == 'identity Proof'){
    options.add(new SelectOption(instance.Name,instance.Name));
    }
    }
 
    return options;
}
 public list<SelectOption> getItems3(){
    List<SelectOption> options = new List<SelectOption>();
    Map<String, ProductDoc__c> custSetting= ProductDoc__c.getAll();
    for(ProductDoc__c instance : custSetting.values()){
    if(instance.Type__c == 'Income Proof'){
    options.add(new SelectOption(instance.Name,instance.Name));
    }
    }
 
    return options;
}

public PageReference Save(){
     string productName=Apexpages.Currentpage().getParameters().get('ProductName');
     string active=Apexpages.Currentpage().getParameters().get('Active');
     string opt_doc=Apexpages.Currentpage().getParameters().get('ReqdDocuments');
     string req_doc=Apexpages.Currentpage().getParameters().get('OptionalDocuments');
     string opt_doc2=Apexpages.Currentpage().getParameters().get('ReqdDocumentsIP');
     string req_doc2=Apexpages.Currentpage().getParameters().get('OptionalDocumentsIP');
     string opt_doc3=Apexpages.Currentpage().getParameters().get('ReqdDocumentsInP');
     string req_doc3=Apexpages.Currentpage().getParameters().get('OptionalDocumentsInP');
    if(editMode==false){
     this.prod  = new Product__c();
     }
     if(active == 'on'){
         this.prod.Active__c = true;
     }
     else{
         this.prod.Active__c = false;
         }
     this.prod.Name = productName;
     this.prod.Required_Product_Doc__c = req_doc;
     this.prod.Optional_Product_doc__c = opt_doc;
     this.prod.Required_Doc_Identity_proof__c = req_doc2;
     this.prod.Optional_Doc_Identity_Proof__c= opt_doc2;
     this.prod.Required_Doc_Income_Proof__c=req_doc3;
     this.prod.Optional_Doc_Income_Proof__c=opt_doc3;
    try {
      upsert(this.prod);
    } catch(System.DMLException e) {
       ApexPages.addMessages(e);
       return null;
    }
       //  After Save, navigate to the default view page:  
       return (new ApexPages.StandardController(this.prod)).view();
     
   }
   
   public PageReference cancelPage(){
   PageReference pref=new PageReference('/a06/o');
   return pref;
   }

public pageReference populateReqdFields(){
      // SelectedItems1 =Apexpages.Currentpage().getParameters().get('selStates');
       //system.assertEquals(SelectedItems2,'abc');
       return null;
   }
}