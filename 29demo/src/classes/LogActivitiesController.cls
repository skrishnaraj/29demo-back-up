public class LogActivitiesController {

    public PageReference back() {
        return (new PageReference('/ui/desktop/DesktopPage'));
    }
    
    public String phoneNumber {get;set;}
    public String call_id{get;set;}
    
    public String noteBody {get;set;}
    public List<Service__c> listOfServices;
    public List<ServiceDetails> serviceDetailsList = new List<ServiceDetails>();

    public PageReference cancelComment() {
    
        return null;// (new PageReference('/ui/desktop/DesktopPage'));
    }

    
    class ServiceDetails{
    public Boolean checked { get; set; }
    public Service__c serv { get; set; }
    
    public ServiceDetails(Service__c s){
        serv = s;
        checked = false;
    }
    
    }
    
    public List<ServiceDetails> getServiceNames(){
    return serviceDetailsList;
    }
    
    public LogActivitiesController(){
        String currentLead = ApexPages.currentPage().getParameters().get('id');
         if(currentLead != null && currentLead != ''){
                leadRecord = [select Id,Name,Lead_Name__c,Lead_Status__c,Owner.Name from Lead__c where Id = :currentLead];
            }
       // service = [select id,Name,Lead__c from service__c where Lead__c=:currentLead];
        for(Service__c s:[select id,Name,Lead__c from service__c where Lead__c=:currentLead]){
            ServiceDetails serDt = new ServiceDetails(s);
            serviceDetailsList.add(serDt);
         }  
    }


private Lead__c leadRecord;
    private Service__c serviceRecord;
    public String activityType { get;set;}
    public String appointmentDate{ get;set;}
    public String activityNotes {get;set;}
    public String userActivity{get;set{ userActivity = value; }}
    

    List<Task> tasksToInsert ;
    Task completedtask;

      public String getLeadName(){
    if( leadRecord != null){
        return leadRecord.Lead_Name__c;
      }
    return null;
    }
     
      void createNewActivity(String status,String subject,String whatId,String description, String dueDate, String priority,String call_id,String callStatus,String userName){
        Task taskToCreate = new Task();
        taskToCreate.WhatId = whatId;
        taskToCreate.Subject = subject;
        taskToCreate.OwnerId = UserInfo.getUserId();
        taskToCreate.Status = status;
        taskToCreate.Description = description;
        taskToCreate.Call_Status__c = callStatus;  
        taskToCreate.Assigned_To__c = userName;
        if(call_id != null){
             taskToCreate.Call_Id__c = call_id;
            }
        
        if(dueDate != null || dueDate != 'null'){
            taskToCreate.ActivityDate = Date.parse(dueDate);
        }
        taskToCreate.Priority = priority;
        tasksToInsert.add(taskToCreate) ;
     }
      
      public String dueDate { get; set; }
    Task tempTask ;
    public Task getTask(){
        tempTask = new Task();
        return tempTask;
    }
    
    public PageReference createActivity(){
        tasksToInsert = new List<Task>();
        String subject;
        if(leadRecord != null){
            if(serviceActivities == true){
                //iterate through every service and create activity history
                for(ServiceDetails tempVar: serviceDetailsList){
                    if(tempVar.checked == true){
                        subject  = 'Contacted  ' + leadRecord.Lead_Name__c + ' for ' + tempVar.serv.Name + ' on ' + date.today().format();
                        createNewActivity('Completed',subject,tempVar.serv.Id,(activityNotes.length()>255)?activityNotes.substring(0,254):activityNotes,Date.today().format(),'Normal',call_id,userActivity,leadRecord.Owner.Name);
                        if(dueDate!=null && dueDate != ''){
                            //create follow-up activity for every service
                            subject  =  'Follow-up for ' + leadRecord.Lead_Name__c + ' (' + tempVar.serv.Name + ')';
                            createNewActivity('Follow-up',subject,tempVar.serv.Id,tempTask.Description,dueDate,'High',null,'Follow-up',leadRecord.Owner.Name);
                        }
                    }   
                }
            }    
            else{
                if(dueDate!=null && dueDate != ''){
                    //create follow-up activity for every service
                    subject  =  'Follow-up for ' + leadRecord.Lead_Name__c ;
                    createNewActivity('Follow-up',subject,leadRecord.Id,tempTask.Description,dueDate,'High',null,'Follow-up',leadRecord.Owner.Name);
                }
                //create lead activity history
                subject  = 'Contacted  ' + leadRecord.Lead_Name__c + ' on ' + date.today().format();
                createNewActivity('Completed',subject,leadRecord.Id,(activityNotes.length()>255)?activityNotes.substring(0,254):activityNotes,Date.today().format(),'Normal',call_id,userActivity,leadRecord.Owner.Name );
            }
            database.insert(tasksToInsert);
        }
        return null;
    }
    
        public Boolean serviceActivities { get; set; }

    public PageReference showServiceTable() {
    serviceActivities = true;
        return null;
    }
    
    public PageReference showServiceTable2() {
    serviceActivities = false;
        return null;
    }
    
}