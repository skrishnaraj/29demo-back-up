public with sharing class SalesRepTrackerController1 {

    public String AppointmentDate { get; 
    set;}
    Date appDate;
    public SalesRepTrackerController1(){
    AppointmentDate = Date.today().format();
    AppDateActive = Date.today().format();
    }
    static String FieldRepId { get; set; } 
    static String AppDateActive { get; set; }
    
    
    public String selectedRep { get; 
    set{
        selectedRep = value;
        }
    }
    
    public String activeSelectedRep { get; 
    set{
        activeSelectedRep = value;
        }
    }

static List<String> locations = new List<String>();
 static List<string> addresses ;
 static List<string> addressAppointmentList ;
 Lead__c tempLead;
 
    public Lead__c getDateFrom(){
        tempLead = new Lead__c();
        return tempLead;
    }

    public List<SelectOption> getSalesReps() {
    List<SelectOption> options = new List<SelectOption>();
    for(FieldRep__c u :[select Id,Name from FieldRep__c where Active__c = true ]){ // ideally there should be a where clause like - where Profile = 'Sales Rep'
        options.add(new SelectOption(u.Id,u.Name));
        }
        return options;
    }
    
    public List<SelectOption> getActiveSalesReps() {
    List<SelectOption> options = new List<SelectOption>();
    String sysAdminProfileId = [select Id from Profile where Name like '%System Administrator'].Id;
    Id manager=UserInfo.getUserId();
    String currentUserProfileId = UserInfo.getProfileId();
    if(sysAdminProfileId == currentUserProfileId ){
        for(FieldRep__c u:[select id,Name from FieldRep__c where Active__c = true]){ // ideally there should be a where clause like - where Profile = 'Sales Rep'
            options.add(new SelectOption(u.Id,u.Name));
        }
    }
    else{
        for(FieldRep__c u:[select id,Name from FieldRep__c where Active__c = true and reporting_manager__c=:manager]){ // ideally there should be a where clause like - where Profile = 'Sales Rep'
            options.add(new SelectOption(u.Id,u.Name));
            }
        }
        return options;
    }
 
    public List<String> getAddresses(){
     if (addresses == null){
        return new List<String>{'null'};
    }
    return addresses;
 }
 
    public PageReference showSchedule() {
        if(addresses == null || addressAppointmentList == null ){
            addresses = new List<String>();
            addressAppointmentList = new List<String>();
        }
        //system.assertEquals(activeSelectedRep,null);
        addresses.clear();
        List<String> whatIds = new List<String>();  
        List<Lead__c> queryResults = new List<Lead__c>(); 
        String leadQuery = '';
                //public List<List<String>> locations = new List<List<String>>();
        appDate = date.parse(AppointmentDate);
        
        if( appDate != null){
            queryResults = [select Id,Home_Country_Address__c,Appointment_date__c,Appointment_Date_Time__c from Lead__c where Field_Rep__c = :activeSelectedRep and  Appointment_date__c = :appDate  order by Appointment_Date_Time__c asc];
        }     
        for(Lead__c lead : queryResults){
        if(lead.Home_Country_Address__c!= null){
        
         String addr = '\''+lead.Home_Country_Address__c+'\'';
            }
        }
        FieldRepId = activeSelectedRep ;
        AppDateActive = AppointmentDate;
        return null;
    } 
    
     public List<String> getLocations(){
     // system.assertEquals(AppointmentDate,null);
     locations.clear();
    //AppDateActive  = '4/9/2012';
     //FieldRepId = 'a06E00000053MUh';
     if(AppDateActive != null){
     appDate = date.parse(AppDateActive);}
    Integer i = 0;
    String PrevLatLong = null;
     if( appDate != null){
      for(Location_Data__c location: [Select id,Latitude__c,Longitude__c,FieldRep__r.Full_Name__c,CapturedDateTime__c from Location_Data__c where FieldRep__c =:FieldRepId and currentDate__c=:appDate and Ignore_Record__c=false order by CapturedDateTime__c]){
       String currentLatLong = '\''+location.Latitude__c+','+location.Longitude__c+'\'';
      if(i==0){
      locations.add(currentLatLong);
      i++;
      }
      else if(i==10){
      break;
      }
      else if(!PrevLatLong.equalsIgnoreCase(currentLatLong)){
      
      locations.add(currentLatLong);
      i++;
      }
      PrevLatLong = currentLatLong ;
      }
      }
     
    //system.assertEquals(locations,null); 
  return locations;
  } 
    
     public List<string> getAppointmentTimes() {

      if (addressAppointmentList == null){
        return new List<String>{'null'};
    }
        return addressAppointmentList;
    }
    
    
}