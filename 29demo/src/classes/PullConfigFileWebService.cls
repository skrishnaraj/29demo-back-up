@RestResource(urlMapping='/PullConfigFileFromServer/*')

global class PullConfigFileWebService{
    

@HttpGet
 global static void getConfigFile() {
 RestRequest req = RestContext.request;
 RestResponse res = RestContext.response;
 String configFileName = req.params.get('Name');
 String timeStamp = req.params.get('Time_Stamp');
 Blob b ;
 Document docResult = [SELECT Id, Name,Body,Url,LastModifiedDate FROM Document WHERE Name=:configFileName];
 if(timeStamp != null && timeStamp != '')
    {
     if(docResult.LastModifiedDate > dateTime.parse(timeStamp)){
         b = docResult.Body;
         res.responseBody = b;
         res.addHeader('Time_Stamp',docResult.LastModifiedDate.format());
     }
     else{
     res.responseBody = Blob.valueOf('');
     res.addHeader('Time_Stamp',docResult.LastModifiedDate.format());
     res.addHeader('Custom_Status_Code','407');
     }
 }
 else{
     b = docResult.Body;
     res.responseBody = b;
     system.debug('Response Body = ' + res.responseBody);
     res.addHeader('Time_Stamp',docResult.LastModifiedDate.format());
 }
 }
}