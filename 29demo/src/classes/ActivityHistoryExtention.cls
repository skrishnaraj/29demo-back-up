public class ActivityHistoryExtention {

    string currentLead;
    public Integer listSize {get;set;}
    public Integer listSizeDefault {get;set;}
    public Boolean renderSection2;
    public Boolean getRenderSection2(){
    return renderSection2;
    }
    public Boolean renderSection1  = true;
    public Boolean getRenderSection1(){
    return renderSection1;
    }
    public Boolean renderSection3  = true;
    public Boolean getRenderSection3(){
    return renderSection3;
    }
    public Boolean renderSection4;
    public Boolean getRenderSection4(){
    return renderSection4;
    }
    public Boolean goto1;
    public Boolean getgoto1(){
    return goto1;
    }
     public Boolean goto2;
    public Boolean getgoto2(){
    return goto2;
    }
    
    List<ActivityHistory> fiveDefaultActivities = new List<ActivityHistory>();
    List<ActivityHistory> defaultActivities = new List<ActivityHistory>();
    List<ActivityHistory> fiveFieldActivities = new List<ActivityHistory>();
    List<ActivityHistory> fieldActivities = new List<ActivityHistory>();
   
    public ActivityHistoryExtention(ApexPages.StandardController controller) {
     currentLead = ApexPages.currentPage().getParameters().get('id');
      String rend1 = ApexPages.currentPage().getParameters().get('renderSection1');
      String rend2 = ApexPages.currentPage().getParameters().get('renderSection2');
      String rend3 = ApexPages.currentPage().getParameters().get('renderSection3');
      String rend4 = ApexPages.currentPage().getParameters().get('renderSection4');
      if(rend1 != null)
      {
      renderSection1 = Boolean.ValueOf(rend1);
      
      }
      if(rend2 != null)
      {
      renderSection2 = Boolean.ValueOf(rend2);
      
      }
      if(rend3 != null)
      {
      renderSection3 = Boolean.ValueOf(rend3);
      
      }
      if(rend4 != null)
      {
      renderSection4 = Boolean.ValueOf(rend4);
      }
      Lead__c leadDetails = [select(select WhatId,OwnerID,ActivityDate,Subject,Status,Phone__c,LastModifiedDate,Assigned_To__c,Priority,Description,Call_Status__c,Duration__c,Call_Id__c,Call_Start_End_Time__c FROM ActivityHistories WHERE Activity_Type__c='Default')from Lead__c WHERE Id=:currentLead];
        listSizeDefault = leadDetails.ActivityHistories.size();
           for(Integer i = 0; i<leadDetails.ActivityHistories.size() ; i++){
               if(i<5){
                   fiveDefaultActivities.add(leadDetails.ActivityHistories[i]);
                   }
                   defaultActivities.add(leadDetails.ActivityHistories[i]);
               }
               

        Lead__c fieldLeadDetails = [select(select WhatId,OwnerID,ActivityDate,Subject,Status,Phone__c,Priority,LastModifiedDate,Assigned_To__c,Description,Call_Status__c,Duration__c,Call_Id__c,Call_Start_End_Time__c FROM ActivityHistories WHERE Activity_Type__c='Field')from Lead__c WHERE Id=:currentLead ];
        listSize = fieldLeadDetails.ActivityHistories.size();

           for(Integer i = 0; i<fieldLeadDetails.ActivityHistories.size() ; i++){
               if(i<5){
               fiveFieldActivities.add(fieldLeadDetails.ActivityHistories[i]);
               }
               fieldActivities.add(fieldLeadDetails.ActivityHistories[i]);
           }
     }
    
      
    public List<ActivityHistory> getActivityFields(){
       
       return defaultActivities;
       
      } 
      public List<ActivityHistory> getActivityFields3(){
       return fiveDefaultActivities;
      } 
      
      
      public List<ActivityHistory> getActivityFields1(){
       return fieldActivities;
       }
       public List<ActivityHistory> getActivityFields2(){
       return fiveFieldActivities;
       }
       
      
      public PageReference renderSections(){
          renderSection1=false;
          renderSection3=false;
          renderSection2=true;
          return null;
      }
      public PageReference HideSection(){
          renderSection1=true;
          renderSection2=false;
          renderSection3=true;
          return null;
      }
      public PageReference renderSections5(){
          renderSection1=false;
          renderSection3=false;
          renderSection4=true;
          return null;
      }
      public PageReference HideSection1(){
          renderSection1=true;
          renderSection4=false;
          renderSection3=true;
          return null;
      }
}