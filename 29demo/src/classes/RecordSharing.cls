public class RecordSharing {

    private final Lead__c leadRecord;
    Lead__c leadRec;
    Map<String,String> fieldMgr_Map = new Map<String,String>();
    
    public RecordSharing(ApexPages.StandardController stdController) {
        renderSection2 = false;
        this.leadRecord = (Lead__c)stdController.getRecord();
        leadRec= [select Id,Lead_Name__c,Assigned_To__c,Share_Record_With__c,InitiationDate__c,Approval_Status__c  from Lead__c where Id = :this.leadRecord.Id];
    }
    
    public RecordSharing(){
        String recId = ApexPages.currentpage().getParameters().get('id');
        if(recId != null){
            leadRec = [select Id,Lead_Name__c,Assigned_To__c,Share_Record_With__c,InitiationDate__c,Approval_Status__c from Lead__c where Id = :recId];
        }
    }
    
    public String selectedManager {get;set{selectedManager = value;}}

    List<SelectOption> options = new List<SelectOption>();    
    public List<SelectOption> getFieldManagers(){
        for(User u : [select Name,Id from User where UserRole.Name = 'Field Managers']){
            options.add(new SelectOption(u.Id,u.Name));
            fieldMgr_Map.put(u.Id,u.Name);
        }
        return options;
    }

    /*
   Public void manualShareRead(){
   ApexPages.Message myMsg ;
    String recordId, userOrGroupId;
    recordId =  leadRec.Id;
    userOrGroupId = selectedManager ;
      // Create new sharing object for the custom object Job. 
      Lead__Share leadShr  = new Lead__Share();
     // Set the ID of record being shared.    
      leadShr.ParentId = recordId;       
      // Set the ID of user or group being granted access.    
      leadShr.UserOrGroupId = userOrGroupId;        
      // Set the access level.     
      leadShr.AccessLevel = 'Edit';        
      // Set rowCause to 'manual' for manual sharing.      
      leadShr.RowCause = Schema.Lead__Share.RowCause.Manual;        

      Database.SaveResult sr = Database.insert(leadShr,false);
      // Process the save results.    
      if(sr.isSuccess()){
         // Indicates success  
         database.update(leadRecord);
        FeedItem post = new FeedItem();
             post.ParentId = userOrGroupId ; //eg. Opportunity id, custom object id..
             post.Body = '#' + leadRec.Assigned_to__c + ' has shared the lead '+ leadRec.Lead_Name__c + ' with you. Please follow-up for a ' + leadRecord.Sharing_Reason__c +'. ';
             post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + leadRecord.Id;
             post.Title = leadRec.Lead_Name__c;
             insert post;
             
             renderSection2 = true;
              myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Record successfully shared');
              ApexPages.addMessage(myMsg);
         //return null;//return true;
      }
      else {
         // Get first save result error.     
         Database.Error err = sr.getErrors()[0];        
         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                  err.getMessage().contains('AccessLevel')){
            // Indicates success. 
            database.update(leadRecord);
            FeedItem post = new FeedItem();
             post.ParentId = userOrGroupId ; //eg. Opportunity id, custom object id..
              post.Body = '#' + leadRec.Assigned_to__c + ' has shared the lead '+ leadRec.Lead_Name__c + ' with you. Please follow-up for a ' + leadRecord.Sharing_Reason__c +'. ';
             post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + leadRecord.Id;
             insert post;
             
             myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Record successfully shared');
              ApexPages.addMessage(myMsg);
              renderSection2 = true;
         }
         else{
         }
       }
   }
   */
   public void criteriaBasedSharing(){
    ApexPages.Message myMsg ;
    leadRec.Share_Record_With__c = fieldMgr_Map.get(selectedManager);
    leadRec.InitiationDate__c = date.today();
    leadRec.Approval_Status__c = 'Open';
      // into the operation.    
      Database.SaveResult sr = Database.update(leadRec,false);
      // Process the save results.    
      if(sr.isSuccess()){
        FeedItem post = new FeedItem();
        database.update(leadRecord);
         post.ParentId = selectedManager ; //eg. Opportunity id, custom object id..
         post.Body = '#' + leadRec.Assigned_to__c + ' has shared the lead '+ leadRec.Lead_Name__c + ' with you. Please follow-up for a ' + leadRecord.Sharing_Reason__c +'. ';
         post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + leadRecord.Id;
         post.Title = leadRec.Lead_Name__c;
         insert post;
         
         renderSection2 = true;
          myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Record successfully shared');
          ApexPages.addMessage(myMsg);
      }
      else {
       }
   }
   
   public Boolean renderSection2 {get;set;}

   // Test for the manualShareRead method 
    /*
   static testMethod void testManualShareRead(){
      // Select users for the test. 
    
      List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
      Id User1Id = users[0].Id;
      Id User2Id = users[1].Id;
   
      // Create new job. 
    
      Lead__c j = new Lead__c();
      j.Name = 'Test Job';
      j.OwnerId = user1Id;
      insert j;    
                
      // Insert manual share for user who is not record owner. 
    
      //System.assertEquals(manualShareRead(j.Id, user2Id), true);
   
      // Query job sharing records. 
    
      List<Lead__Share> jShrs = [SELECT Id, UserOrGroupId, AccessLevel, 
         RowCause FROM Lead__Share WHERE ParentId = :j.Id AND UserOrGroupId= :user2Id];
      
      // Test for only one manual share on job. 
    
      System.assertEquals(jShrs.size(), 1, 'Set the object\'s sharing model to Private.');
      
      // Test attributes of manual share. 
    
      System.assertEquals(jShrs[0].AccessLevel, 'Read');
      System.assertEquals(jShrs[0].RowCause, 'Manual');
      System.assertEquals(jShrs[0].UserOrGroupId, user2Id);
      
      // Test invalid job Id. 
    
      delete j;   
   
      // Insert manual share for deleted job id.  
    
      //System.assertEquals(manualShareRead(j.Id, user2Id), false);
   }  
   */
}