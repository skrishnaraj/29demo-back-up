global virtual class LeadRecords{
    public String Name{get; set;}
    public String Home_Country_Address{get; set;}
    public String ProductsOfInterest{get; set;}
    public String Id{get; set;}
    public String Appointment_Date_Time{get; set;}
    public String Phone {get;set;}
    public String Occupation {get;set;}
    public List<LeadDocument> documentsList{get;set;}
    public String ActivityName { get;set;}
    public String SR_Appointment_Status {get;set;}
    public String SR_Appointment_Comment {get;set;}
    public String Latitude{get;set;}
    public String Longitude{get;set;}
    public String IMEI_Number{get;set;}
    public String CapturedDateTime{get;set;}
 

global class LeadDocument{
    public String Name{get;set;}
    public Boolean Mandatory{get;set;}
    public Decimal numberOfImages {get;set;} 
    public String FilePath {get;set;}
    public String Category {get;set;}
}

}