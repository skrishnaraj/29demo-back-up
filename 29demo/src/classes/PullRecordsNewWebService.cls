@RestResource(urlMapping='/PullDataFromServer2/*')
global class PullRecordsNewWebService {

global class MobileData{
    public String Time_Stamp {get;set;}
    public List<Map<String,String>> records{get;set;}
    
}
global class documentsList{
    public String Name{get;set;}
    public Boolean Mandatory{get;set;}
    public Decimal numberOfImages {get;set;} 
    public String FilePath {get;set;}
    public String Category {get;set;}
}
   
    
   @HttpGet
   global static void getRecords() {
    Map<String,Map<String,String>> leadIdFieldsMap = new Map<String,Map<String,String>>();
    Map<String,List<documentsList>> leadIdDocsMap = new Map<String,List<documentsList>>();
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   String appDateString = req.params.get('Appointment_Date');
   String IMEI_Number= req.params.get('IMEI_Number');
   
   String timeStamp = req.params.get('Time_Stamp');
   String tempDateTime;
   if(timeStamp != null){
    tempDateTime = Date.today().format() + '' + timeStamp.substring(timeStamp.length()-9, timeStamp.length());
   }
   DateTime timeStampDate;
   Date appointmentDate;
   try{
    //Date t = ;
    if(dateTime.now() >dateTime.parse(date.today().format()+ ' 7:59 PM') || dateTime.now() < dateTime.parse(date.today().format()+ ' 9:00 AM')  ){
            res.addHeader('Custom_Status_Code', '407');
            //return null;
        }
    if(timeStamp != null){
        timeStampDate = dateTime.parse(tempDateTime);
    }
    //appointmentDate=date.parse(appDateString);
    appointmentDate=date.today();
   }
   catch(Exception ex){
        res.addHeader('Custom_Status_Code', '401');
        //return null;
   } 
   
   
   List<Map<String,String>> leadRecordList= new List<Map<String,String>>();
   List<Lead__c> resultsList = new List<Lead__c>();
    String query ;
    String fieldRepId;
    try{
        fieldRepId = [select Id from FieldRep__c where IMEI_Number__c = :IMEI_Number and Active__c = true].Id;
    }
    catch(Exception e){
        if(e.getMessage().contains('List has no rows for assignment')){
            res.addHeader('Custom_Status_Code', '400');
            //return null;
        }
     }
     Set<String> uniqueFields = new Set<String>();
     Map<String,List<String>> activityFieldsMap = new Map<String,List<String>>(); 
     List<String> activityFieldsList = new List<String>();
     String activityName = '';
     for(Config_Activities__c configValue : Config_Activities__c.getAll().values()){
        if(configValue.Unique_Name__c != 'documentsList'){
            String testField = configValue.Unique_Name__c.toUpperCase();
            uniqueFields.add(testField);
        }
        if(activityFieldsMap.get(configValue.Activity_Name__c) == null){
            List<String> temp = new List<String>();
            temp.add(configValue.Unique_Name__c);
            activityFieldsMap.put(configValue.Activity_Name__c, temp);
        }
        else{
            List<String> temp = activityFieldsMap.get(configValue.Activity_Name__c);
            temp.add(configValue.Unique_Name__c);
            activityFieldsMap.put(configValue.Activity_Name__c,temp);
        }

     }
     //add the last activity and corresponding list
     //activityFieldsMap.put(activityName,activityFieldsList);
     system.debug(activityFieldsMap + 'trial');
     Map<String,String> recordData = new Map<String,String>();
     String soql = 'Select ';
     for(String field : uniqueFields){
        soql = soql + field + ', ';
     }
     soql = soql + 'RecordType.Name,(Select Id,Name,Document_Name__c,Number_Of_Pages__c,Mandatory__c From ScannedDocuments__r) from Lead__c where Appointment_date__c = ' + String.valueOf(date.today()) + ' AND Field_Rep__c = \'' + fieldRepId + '\'';
    if(timeStampDate != null){
        
    }
    system.debug(soql + 'TEEST');
    
   for(Lead__c l: database.query(soql))
   {
     
  
        List<documentsList> tempStorage = new List<documentsList>(); 
        List<String> queriedData = activityFieldsMap.get(l.RecordType.Name);
        if(queriedData !=null){
        recordData = new  Map<String,String>();
            for(String qdata:queriedData){
            
                if(qData != 'documentsList'){
                    if(l.get(qdata) != null){
                        recordData.put(qdata.split('__c')[0],String.valueOf(l.get(qdata)));
                    }
                    else{
                        recordData.put(qdata.split('__c')[0],'null');
                    }
                }
                else{
                    
                    for(Integer i = 0; i< l.ScannedDocuments__r.size();i++){
                        documentsList d = new documentsList();
                        d.Mandatory = l.ScannedDocuments__r[i].Mandatory__c;
                        d.numberOfImages = l.ScannedDocuments__r[i].Number_Of_Pages__c;
                        d.Name = l.ScannedDocuments__r[i].Document_Name__c;
                        tempStorage.add(d);
                    }
                    //recordData.put('documentsList',tempStorage);
                }
            }
            recordData.put('ActivityName',l.RecordType.Name);       
        leadRecordList.add(recordData);
        leadIdFieldsMap.put(l.Id,recordData);
        leadIdDocsMap.put(l.Id,tempStorage); 
        }
   }
    //system.assertEquals(leadIdFieldsMap,null);
    JSONGenerator gen = JSON.createGenerator(true);
    // Write data to the JSON string.
    gen.writeStartObject();
    
    gen.writeStringField('Time_Stamp', datetime.now().format());
    gen.writeFieldName('records');
    gen.writeStartArray();
    for(String lead : leadIdFieldsMap.keySet()){
        gen.writeStartObject();
        for(String key: leadIdFieldsMap.get(lead).keySet()){
            gen.writeObjectField(key, leadIdFieldsMap.get(lead).get(key) );
        }
        gen.writeFieldName('documentsList');
        gen.writeObject(leadIdDocsMap.get(lead));
        gen.writeEndObject();
    }
    gen.writeEndArray();
    gen.writeEndObject();
    //
   String genString = gen.getAsString().replace('\n','');
    genString = genString.replace('\"', '\'');
    //genString = genString.replace('\'', '"');
    //genString = genString.replace('}"', '}');
   //return genString;
   system.debug(genString);
    res.responseBody = Blob.valueOf(genString);
 }
}