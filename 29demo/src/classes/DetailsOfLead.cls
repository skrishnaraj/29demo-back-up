public with sharing class DetailsOfLead {
    
    String url;
    
    
    public String getUrl() {
    
        return url;
    }
     public String FieldRepId {get;set{
    FieldRepId = value;
    }}

    public PageReference assign() {
     FieldRepId = ApexPages.currentPage().getParameters().get('fieldRepToAssign');
    List<Lead__c> newList = new List<Lead__c>();
    for(Lead__c ser:listOfLeads){
    if(ser.id==leadid){
       ser.Field_Rep__c = FieldRepId;
      newList.add(ser);
     }
    }
    //system.assertEquals(newList,null);
    database.update(newList);
    return null;
    }


    

    public PageReference closeZipCodeSection() {
        renderSearchSection = false;
        return null;
    }


    public Boolean renderForUnassigned { get; set; }

public String zipCodeValue {get;set;}
public Boolean renderSearchSection {get;set;}

   /* public PageReference renderSearch(){
    String ZipcodeAndDate = ApexPages.currentPage().getParameters().get('zipCodeValue');
    //system.assertEquals(ZipcodeAndDate,null);
   String[] temp = new String[]{};
   temp = ZipcodeAndDate.split(';');
   Boolean b = true;
   String s = 'Some Error Please Refresh the Page and try Again';
   String s1 = 'Appointment Date is Null ';
   for(Integer i=0;i<4;i++)
   {
     if(temp[i].length() > 0)
     {
      if(i == 0)
      {
      leadid = temp[i];
      }
      if(i == 1)
      {
      Appointmentdate = temp[i];
      }
      if(i == 2)
      {
      ZipCode = temp[i];
      }
     }
     else{
     if(i==0)
     {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,s));
       b = false;
     }
     if(i==1)
     {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,s1));
       b = false;
     }
         
     }
   
   }
  
    if(b){
    renderSearchSection = true;
    //system.assertEquals(Appointmentdate, null);
    d= date.parse(Appointmentdate);
    }
    
    
    //clearError();
    return null;
    } */
String RecordTypeName ; 
public DetailsOfLead(){
 renderSearchSection = false;
 renderForUnassigned  = true;
 
 string profileId = UserInfo.getProfileId();
 Profile p = [select id,Name from profile where id=:profileId];
 for(CollectionProfile__c c:[Select id,Name from CollectionProfile__c])
 {
    //system.assertEquals(c, null);
 if(c.Name == p.Name)
 {
    
    RecordTypeName = 'Collection';
 }
 }
 for(SalesProfile__c c:[Select id,Name from SalesProfile__c])
 {
 if(c.Name == p.Name)
 {
    RecordTypeName = 'Sales';
 }
 } 
//system.assertEquals(RecordTypeName, null);
 customQuery = 'select id,Name,Lead_Name__c,Appointment_date__c,Field_Rep__c,Street__c,Appointment_Time_From__c,Appointment_Time_To__c,City__c,State_Province__c,Phone__c,Home_Country_Address__c,Home_Country_District__c,Home_Country_State__c,Zipcode__c from Lead__c where RecordType.Name = \''+RecordTypeName+'\'' ;
 soql = customQuery + ' and Field_Rep__c = null';
 originalSOQL = soql;
 }
    
   public PageReference cancel() {
        return null;
    }
    //Map<String, Schema.RecordTypeInfo> mapset = Lead__c.SObjectType.getDescribe().getRecordTypeInfosByName();
 string originalSOQL;// = 'select id,Name,Lead_Name__c,Appointment_date__c,Field_Rep__c,Street__c,City__c,State_Province__c,Phone__c,Zipcode__c from Lead__c where Field_Rep__c = null and RecordType.Name = \''+RecordTypeName+'\'';
    public PageReference save() {
        update listOfLeads;
        con = null;
        return null;
    }
// private String soql = 'select id,Name,Lead_Name__c,Appointment_date__c,Field_Rep__c,Street__c,City__c,State_Province__c,Phone__c,Zipcode__c from Lead__c where Field_Rep__c =null and RecordType.Name = \''+RecordTypeName+'\'' ; // and Appointment_Date__c != null order by Appointment_Date__c'
 String customQuery;
 private String soql;// = customQuery + ' and Field_Rep__c = null';
 
 public String selectOpt{get;set{selectOpt=value;}} 
 public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;}
    set;
  }
 public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }

  List<Lead__c> listOfLeads = new List<Lead__c>();
 
    public List<Lead__c> getLeads(){
        listOfLeads.clear();
        for(Lead__c leadRec : (List<Lead__c>)con.getRecords())
        {
            listOfLeads.add(leadRec);
        }
        return listOfLeads;
    }
    
    public PageReference records(){
       con = null;
       soql = null;
        if(selectOpt== 'Unassigned'){
                
                soql = customQuery +' and Field_Rep__c=null and Appointment_Date__c != null  order by Appointment_Date__c'  ;
               // soql = 'select id,Name,Lead_Name__c,Appointment_date__c,Field_Rep__c,Street__c,City__c,State_Province__c,Phone__c,Zipcode__c from Lead__c where Field_Rep__c=null and Appointment_Date__c != null  order by Appointment_Date__c';
                originalSOQL = soql;
                renderForUnassigned = true;
                }
        else if(selectOpt == 'Assigned'){
                soql = customQuery + ' and Field_Rep__c != null  order by Appointment_Date__c';
                //soql = 'select id,Name,Appointment_date__c,Lead_Name__c,Field_Rep__c,Street__c,City__c,State_Province__c,Phone__c,Zipcode__c from Lead__c where Field_Rep__c != null  order by Appointment_Date__c';
                originalSOQL = soql;
                renderForUnassigned = false;
                renderSearchSection  = false;
        }

            return null;
    }
    public PageReference toggleSort() {

    listOfLeads.clear();
    con = null;
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
     soql = originalSOQL.split('order')[0] + ' order by ' + sortField + ' ' + sortDir ;
     return null;
  }
 

  public ApexPages.StandardSetController con {
        get {

            if(con == null) {
                //system.assertEquals(soql, null);
                con = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
                // sets the number of records in each page set
                con.setPageSize(5);
            }
            return con;
        }
        set;
    }
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
        if(con != null)
            return con.getHasPrevious();
         else return false;
        }
        set;
    }
 
    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
 
    // returns the first page of records
    public void first() {
        con.first();
    }
 
    // returns the last page of records
    public void last() {
        con.last();
    }
 
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
 
    // returns the next page of records
    public void next() {
        con.next();
    }
    ////////////////////////////
    public String leadid{get;set;}
    public String Zipcode { get; set; }
    Date d;
    public String Appointmentdate { get; set; }
    public Boolean show=false;
    public Boolean getShow(){
      return show;
      }
    public List<Lead__c> leadDetails;
   

   /* public List<Lead__c> getLeadDetails(){
        if(ZipCode == null){
            leadDetails=[select ID,Field_Rep__c,Appointment_date__c from Lead__c where Appointment_date__c=:d and Field_Rep__c != null ];
        }
        else{
            leadDetails=[select ID,Field_Rep__c,Appointment_date__c from Lead__c where Zipcode__c =:Zipcode AND Appointment_date__c=:d and Field_Rep__c != null ];
        } */
       /* if(leadDetails == null || leadDetails.isEmpty() )
        {
        String s='There are no records for this day';
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,s));
        } */
       /*  Set<Lead__c> listOfService = new Set<Lead__c>();
       Set<String> setOfFieldRepNames = new Set<String>();
       
       for(Lead__c ser:leadDetails)
       {
       if(!setOfFieldRepNames.contains(ser.Field_Rep__c)){
           listOfService.add(ser);
           setOfFieldRepNames.add(ser.Field_Rep__c);
       }
        
       }
       leadDetails.clear();
        leadDetails.addAll(listOfService);
        return leadDetails;
    } */
    
    // Vivek added the class 19/7
     public PageReference showmapPage() {
     String fieldRepDetails = ApexPages.currentPage().getParameters().get('fieldRepDetails');
     //url ='/apexcomponent/Map_component?FieldRep='+fieldRepDetails;
     PageReference pg = new PageReference('/apex/FieldRepAppointmentMap?FieldRep='+fieldRepDetails);
     //PageReference pg = new PageReference('/apexcomponent/Map_component');
    pg.setRedirect(true);
       return pg;
       //system.assertEquals(url,'abc');
      return null;
    } 
 
}