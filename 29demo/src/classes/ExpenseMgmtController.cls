public class ExpenseMgmtController{
    
    travel_Data__c dummyRecord ;
    List<Id> userIds = new List<Id>();
    Id currentUser = null;
    Double currentUserCommission = 0;
    Double currentUserDistance = 0;
    Double totalDistance_currentUser = 0;
    Double totalDistance_currentUserN = 0;
    Map<String,Double> userCommissionMap = new Map<String,Double>();
    Map<String,Double> userDistanceMap = new Map<String,Double>();
      
    static Boolean firstRun = true; 
    public String selectedUser{get;set{selectedUser=value;}}  
    private List<Travel_Data__c> travelDataRows = new List<Travel_Data__c>();
    public Double commission {get;set;}
    Double total_distance = 0;
    List<User> users;
    
    public String toDateString { get; set; }
    public String fromDateString { get; set; }
    
    Date fromDate;
    Date toDate;
    
    public String RepName { get; set; }
    
    public Boolean buttonChange= false;

    public Boolean getButtonChange(){
    return buttonChange;
    }

    public Boolean section2= false;

    public Boolean getSection2(){
    return section2;
    }
    
    public Boolean section3= false;

    public Boolean getSection3(){
    return section3;
    }

    public travel_Data__c getRecord(){
     return dummyRecord;
     }
    
    public ExpenseMgmtController(){
    if(firstRun == true){
        dummyRecord=new travel_Data__c();
        firstRun = false;
    //renderOnUi='false';
    }
     fromDateString = Date.today().toStartOfMonth().format();
     toDateString = Date.today().toStartOfMonth().addMonths(1).addDays(-1).format();
    Id uid=UserInfo.getUserId();
    List<String> UserIds =new List<String>();
    for(Profile p: [select id from Profile where name='system administrator' or name='Extentor Field managers'])
    {
    UserIds.add(p.id);
    }
   // List<User> u=[SELECT id,Name FROM User where ProfileId IN :UserIds];
    for(User us:[SELECT id,Name FROM User where ProfileId IN :UserIds])
    {
        if(uid == us.Id)
        {
            buttonChange=true;
        }
    }
}

    public PageReference cancel() {
        return null;
    }

    public PageReference cancelAction() {
        return null;
    }

    public PageReference save() {
        update(travelDataRows);
        return null;
    }

    public PageReference edit() {
        return null;
    }

   
    public PageReference changeSetting() {
    PageReference pd = new PageReference('/apex/TAManagement');
        return pd;
    }

  
      public List<UserDetails> getUsers() {
        String userQuery = '';
        userDetails.clear();
        if(selectedUser == 'All'){
                userQuery = 'select id,Name from FieldRep__c where Active__c = true';
        }
        else if(selectedUser != null){
                userQuery = 'select id,Name from FieldRep__c where Id = \'' + selectedUser + '\' And Active__c = true';
        }
        else{
                return null;
        }
          for(FieldRep__c u :database.query(userQuery)){
                  userIds.add(u.Id);
                  UserDetails ud = new UserDetails();
                  ud.userName =u.Name;
                  ud.userId = u.Id;
                  ud.userCommission = 0.0;
                  userDetails.add(ud);
              }
              List<Travel_Data__c> dataRows = [select FieldRep__r.Name,Final_Adjustment__c,Travel_Date__c  from Travel_Data__c where FieldRep__c IN :userIds and Travel_Date__c >= :fromDate and Travel_Date__c <= :toDate order by FieldRep__c ];
              calculateCommission(dataRows);
              calculateDistance(dataRows);
            return userDetails;
        }
    //vivek adds    
        public List<SelectOption> getSelUser() {
    List<SelectOption> options = new List<SelectOption>();
    for(FieldRep__c u :[select Id,Name from FieldRep__c where Active__c = true ]){ // ideally there should be a where ctravelDataRowsuse like - where Profile = 'Sales Rep'
        options.add(new SelectOption(u.Id,u.Name));
        }
        return options;
    }
   
     public PageReference search() {
    
     if(fromDateString !=null){
      fromDate=Date.parse(fromDateString);
      }
      if(toDateString != null)
      {
      toDate=Date.parse(toDateString);
      }
     
       //renderOnUi = 'false';
       section2 = true;
       section3 = false;
        return null;
    }
     // vivek add ends    
        
  public class UserDetails
  {
      public String userName {get;set;}
      public String userId {get;set;}
      public Double userCommission {get;set;}
      public Double userDistance {get;set;}
      
  }
  
  public List<UserDetails> userDetails= new List<UserDetails>();
        
  
  
    public List<Travel_Data__c> getTravel_Data() {
        return travelDataRows;
    }
    
    public PageReference captureUser() {
        travelDataRows.clear();
        section3=true;
        selectedUser=Apexpages.currentPage().getParameters().get('selectedUser');
        repName=[select Name from FieldRep__c where id=:selectedUser].Name;
        travelDataRows = [select FieldRep__r.Name,distance__c,Travel_Date__c,Adjusment_Sign__c,Adjusment__c,Final_Adjustment__c from Travel_Data__c where FieldRep__c=:selectedUser and Travel_Date__c >= :fromDate and Travel_Date__c <= :toDate];
        calculateCommission(travelDataRows);
        return null;
    }
    
    
    /*calculate Commission method*/
    void calculateCommission(List<Travel_Data__c> dataRows){
        userCommissionMap.clear();
        currentUserCommission = 0;
                totalDistance_currentUser = 0;
                currentUser = null;
        TA__c instance = [select id,Name,To_Date__c,From_Date__c,TA_Value__c From TA__c where From_Date__c<=:fromDate and To_Date__c>=:toDate];
        for(Travel_Data__c  row : dataRows){
          if(currentUser != row.FieldRep__c)
          {
              totalDistance_currentUser = row.Final_Adjustment__c ;
              currentUserCommission = 0;
              currentUser = row.FieldRep__c;
          }
          else // same user as previous row
          {
              totalDistance_currentUser = totalDistance_currentUser + row.Final_Adjustment__c ;
             // Daily_TA__c instance = Daily_TA__c.getOrgDefaults();
             // currentUserCommission = totalDistance_currentUser * instance.TA_Value__c; 
             
             currentUserCommission = totalDistance_currentUser * instance.TA_Value__c;
              if(userCommissionMap.containsKey(currentUser)){
                userCommissionMap.remove(currentUser);
              }
              userCommissionMap.put(currentUser,currentUserCommission);
          }          
        }
        List<UserDetails> temp = new List<UserDetails>();
       for(UserDetails u : userDetails){
        u.userCommission = userCommissionMap.get(u.userId);
        temp.add(u);
       }
       //system.assertEquals(userCommissionMap,null);
       userDetails.clear();
       userDetails.addAll(temp);
    } 
    
    /* calculate distance method */
    void calculateDistance(List<Travel_Data__c> dataRows){
     userDistanceMap.clear();
     currentUserDistance = 0;
    totalDistance_currentUser=0;
    currentUser = null;
    for(Travel_Data__c  row : dataRows){
          if(currentUser != row.FieldRep__c)
          {
              totalDistance_currentUser = row.Final_Adjustment__c ;
              //currentUserCommission = 0;
              currentUser = row.FieldRep__c;
          }
           else // same user as previous row
          {
              totalDistance_currentUser = totalDistance_currentUser + row.Final_Adjustment__c ;
             // Daily_TA__c instance = Daily_TA__c.getOrgDefaults();
              currentUserDistance = totalDistance_currentUser ;
              if(userDistanceMap.containsKey(currentUser)){
                userDistanceMap.remove(currentUser);
              }
              userDistanceMap.put(currentUser,currentUserDistance);
          } 
        }
       List<UserDetails> temp = new List<UserDetails>();
       for(UserDetails u : userDetails){
        u.userDistance = userDistanceMap.get(u.userId);
        temp.add(u);
       }
       userDetails.clear();
       userDetails.addAll(temp);
    } 
    
    public PageReference showmapPage() {
    String fieldRepDetails = ApexPages.currentPage().getParameters().get('fieldRepDetails');
    PageReference pg = new PageReference('/apex/FieldRepTravelMap?FieldRep='+fieldRepDetails);
     pg.setRedirect(true);
       return pg;
    } 
}