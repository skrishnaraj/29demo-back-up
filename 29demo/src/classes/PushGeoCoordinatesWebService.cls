@RestResource(urlMapping='/PushGeoCoordinates/*')
global class PushGeoCoordinatesWebService{

@HttpPost   
  global static String pushCoordinates() {
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   String latitude= req.params.get('Latitude');
   String longitude= req.params.get('Longitude');
   String IMEI_Number= req.params.get('IMEI_Number');
   String capturedDateTime = req.params.get('Captured_Date_Time');
   
     Location_Data__c record = new Location_Data__c();
     record.Latitude__c = latitude;
     record.Longitude__c = longitude;
     try{
     record.FieldRep__c = [select Id from FieldRep__c where IMEI_Number__c = :IMEI_Number and Active__c = true].Id;
     //08/28/2012 02:28 PM
     String tempDateTime = Date.today().format() + '' + capturedDateTime.substring(capturedDateTime.length()-9, capturedDateTime.length());
     record.CapturedDateTime__c = DateTime.parse(tempDateTime);
     insert record;
     }
     catch(Exception e){
        if(e.getMessage().contains('List has no rows for assignment')){
            res.addHeader('Custom_Status_Code', '400');
            return 'Custom_Status_Code:400';
        }
        else if(e.getMessage().contains('Invalid date/time')){
            res.addHeader('Custom_Status_Code', '401');
            return null;
        }
        else {
            res.addHeader('Custom_Status_Code', '403');
            return null;
        }
     }
     res.addHeader('Custom_Status_Code', '200');
  return null;
  }
  
  }