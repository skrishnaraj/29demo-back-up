public class MobileConsoleController {

  


    public Config__c config;
    public String myJson;
    
    public class ActivitySorted{
    
    public String FieldLabel;
    public String uniquename;
    public String DataType;
    public Boolean ReadOnly;
    public Boolean Visible;
    public Integer length;
    public String FieldToCompare;
    public String Operator;
    public String ValidationFormat;
    public String Errormessage;
    public String RecordType;
    public String value;
    public String DefaultValue;
    
    }
    
    public List<ActivitySorted> salesActivityFields = new List<ActivitySorted>();
    public List<ActivitySorted> newActivityFields = new List<ActivitySorted>();
    public List<ActivitySorted> collectionActivityFields = new List<ActivitySorted>();
    public List<ActivitySorted> verificationActivityFields = new List<ActivitySorted>();
    
    
    public MobileConsoleController() {
    config = [select id,Name from Config__c where id=:'a0dE0000000GjoD'];
   // config = [select id,Name from Config__c order by CreatedDate desc Limit 1];
    GeneralConfig__c genConfig = [select id,Name,category__c,BucketName__c,End_Time__c,Start_Time__c,AccessKeyId__c,SecretAccessKey__c,ImageResolution__c,LocationIntervalRate__c,NoOfUploadRetry__c,refreshrate__c,UploadRetryInterval__c from GeneralConfig__c where Config__c=:config.id];
    List<errorMessage__c> statusCode = [select id,Name,text__c from errorMessage__c];
    list<Activity__c> activityList = [select id,Name,DataType__c,Picklist_Values__c,Defulat_Picklist_Value__c,Errormessage__c,FieldToCompare__c,length__c,Operator__c,ReadOnly__c,uniquename__c,ValidationFormat__c,Visible__c,Record_Type__c from Activity__c where Config__c =:config.id  ];
     
    for(Activity__c act : activityList)
    {  
     ActivitySorted actSort = new ActivitySorted();
     actSort.FieldLabel = act.Name;
     actSort.uniquename = act.uniquename__c;
     actSort.DataType = act.DataType__c;
     actSort.ReadOnly = act.ReadOnly__c;
     actSort.Visible = act.Visible__c;
     actSort.length = Integer.valueOf(act.length__c);
     actSort.FieldToCompare = act.FieldToCompare__c;
     actSort.Operator = act.Operator__c;
     actSort.ValidationFormat = act.ValidationFormat__c;
     actSort.Errormessage = act.Errormessage__c;
     actSort.RecordType = act.Record_Type__c;
     actSort.value = act.Picklist_Values__c;
     actSort.DefaultValue = act.Defulat_Picklist_Value__c;
      
      if(act.Record_Type__c == 'Sales'){
       salesActivityFields.add(actSort);
      }
      if(act.Record_Type__c == 'New'){
       newActivityFields.add(actSort);
      }
      if(act.Record_Type__c == 'Collection'){
       collectionActivityFields.add(actSort);
      }
      if(act.Record_Type__c == 'Verification'){
       verificationActivityFields.add(actSort);
      }
    
    }
    
    
    
    myJson = '{ \n "Config":{ \n "GeneralConfig":{ \n \"ConfigName\":\"'+genConfig.Name+'\",\n \"UploadRetryInterval\":\"'+genConfig.UploadRetryInterval__c+'\", \n  \"NoOfUploadRetry\": \"'+genConfig.NoOfUploadRetry__c+'\",\n \"ImageResolution\":\"' 
              +genConfig.ImageResolution__c+'\",\n \"refreshrate\":\"'+genConfig.refreshrate__c+'\",\n \"LocationIntervalRate\":\"'+genConfig.LocationIntervalRate__c+'\", \n \"AmazonCredentials\":{ \n \"AccessKeyId\":\"'+genConfig.AccessKeyId__c+'\", \n \"SecretAccessKey\":\"'+genConfig.SecretAccessKey__c+'\", \n \"BucketName\":\"'+genConfig.BucketName__c+'\" \n }, \n \"BusinessHours\":{ \n \"StartTime\":\"' +genConfig.Start_Time__c+ '\" ,\"EndTime\": \"' +genConfig.End_Time__c+'\" \n }, \n \"PictureCategory\": { \"category\":[ '+genConfig.category__c+' \n] \n},  \n \"Custom_Status_Code\":[ \n';
    
    Integer custom = statusCode.size();
    
    for(errorMessage__c con:statusCode)
     {
      
      myJson = myJson+'{ \n \"Code\":\"'+con.Name+'\",\n \"text\":\"'+con.text__c+'\"'; 
      if(custom == 1){
      myJson = myJson+' \n } \n';
      }else{
       myJson = myJson+'\n },\n';
     } 
     custom--;    
     }
     myJson = myJson+'] \n }, \n "Activities":{ \n "Activity":[  \n';
     
     Integer i = 0;
     String currentRecordType;
     String prevRecordType = null;
     // Activity Fields
    /* for(Activity__c act :activityList ){
     
     currentRecordType  = act.Record_Type__c;
     if(currentRecordType != prevRecordType){
       myJson = myJson+'\"Name\":\"'+currentRecordType+'\",\n';
     }
     prevRecordType = currentRecordType;
     myJson = myJson+'\"Field\":[ \n { \"FieldLabel\":\"'+act.Name+'\",\n \"uniquename\":\"'+act.uniquename__c+'\", \n \"DataType\":\"'+act.DataType__c+'\", \n \"ReadOnly\":\"'+act.ReadOnly__c+'\", \n \"Visible\":\"'+act.Visible__c+'\", \n \"length\":\"'+act.length__c
              +'\", \n \"validation\": { \n \"FieldToCompare\":\"'+act.FieldToCompare__c+'\", \n \"Operator\":\"'+act.Operator__c+'\",\n \"ValidationFormat\":\"'+act.ValidationFormat__c+'\", \n \"Errormessage\":\"'+act.Errormessage__c+'\",\n } \n },';
     }
     */
     // Activity field End
     custom = salesActivityFields.size();
     myJson = myJson+'{ \n \"Name\":\"Sales\", \n \"Field\": [  \n';
     for(ActivitySorted sales : salesActivityFields){
       if(true){
         if(sales.DataType == 'PICKLIST'){
          myJson = myJson+'{ \n \"FieldLabel\": \"'+sales.FieldLabel+'\",\n \"uniquename\":\"'+(sales.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+sales.DataType+'\", \n \"Value\": ['+sales.value+' ],\n \"Default\":\"'+sales.DefaultValue+'\", \n \"ReadOnly\":\"'+sales.ReadOnly+'\", \n \"Visible\":\"'+sales.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+sales.FieldToCompare+'\", \n \"Operator\":\"'+sales.Operator+'\",\n \"ValidationFormat\":\"'+sales.ValidationFormat+'\", \n \"Errormessage\":\"'+sales.Errormessage+'\" }';
         }
         else if(sales.DataType == 'IMAGE' || sales.DataType == 'DATE'){
         myJson = myJson+'{ \n \"FieldLabel\": \"'+sales.FieldLabel+'\",\n \"uniquename\":\"'+(sales.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+sales.DataType+'\", \n \"ReadOnly\":\"'+sales.ReadOnly+'\", \n \"Visible\":\"'+sales.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+sales.FieldToCompare+'\", \n \"Operator\":\"'+sales.Operator+'\",\n \"ValidationFormat\":\"'+sales.ValidationFormat+'\", \n \"Errormessage\":\"'+sales.Errormessage+'\" }';
         }
         else{
         myJson = myJson+'{ \n \"FieldLabel\": \"'+sales.FieldLabel+'\",\n \"uniquename\":\"'+(sales.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+sales.DataType+'\", \n \"ReadOnly\":\"'+sales.ReadOnly+'\", \n \"Visible\":\"'+sales.Visible+'\", \n \"length\":\"'+sales.length+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+sales.FieldToCompare+'\", \n \"Operator\":\"'+sales.Operator+'\",\n \"ValidationFormat\":\"'+sales.ValidationFormat+'\", \n \"Errormessage\":\"'+sales.Errormessage+'\" }';
         }
       }
       if(true){
        
        if(custom == 1){
           myJson = myJson+' \n } \n';
         }else{
           myJson = myJson+'\n },\n';
       }
       }
     
     custom--; 
        
     }
     myJson = myJson+'] \n }, \n';
     custom = newActivityFields.size();
     myJson = myJson+'{ \n \"Name\":\"New\", \n \"Field\": [  \n';
     for(ActivitySorted newFields : newActivityFields){
        if(true){
         if(newFields.DataType == 'PICKLIST'){
          myJson = myJson+'{ \n \"FieldLabel\": \"'+newFields.FieldLabel+'\",\n \"uniquename\":\"'+(newFields.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+newFields.DataType+'\", \n \"Value\": ['+newFields.value+' ],\n \"Default\":\"'+newFields.DefaultValue+'\", \n \"ReadOnly\":\"'+newFields.ReadOnly+'\", \n \"Visible\":\"'+newFields.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+newFields.FieldToCompare+'\", \n \"Operator\":\"'+newFields.Operator+'\",\n \"ValidationFormat\":\"'+newFields.ValidationFormat+'\", \n \"Errormessage\":\"'+newFields.Errormessage+'\" }';
         }
         else if(newFields.DataType == 'IMAGE' || newFields.DataType == 'DATE'){
         myJson = myJson+'{ \n \"FieldLabel\": \"'+newFields.FieldLabel+'\",\n \"uniquename\":\"'+(newFields.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+newFields.DataType+'\", \n \"ReadOnly\":\"'+newFields.ReadOnly+'\", \n \"Visible\":\"'+newFields.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+newFields.FieldToCompare+'\", \n \"Operator\":\"'+newFields.Operator+'\",\n \"ValidationFormat\":\"'+newFields.ValidationFormat+'\", \n \"Errormessage\":\"'+newFields.Errormessage+'\" }';
         }
         else{
         myJson = myJson+'{ \n \"FieldLabel\": \"'+newFields.FieldLabel+'\",\n \"uniquename\":\"'+(newFields.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+newFields.DataType+'\", \n \"ReadOnly\":\"'+newFields.ReadOnly+'\", \n \"Visible\":\"'+newFields.Visible+'\", \n \"length\":\"'+newFields.length+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+newFields.FieldToCompare+'\", \n \"Operator\":\"'+newFields.Operator+'\",\n \"ValidationFormat\":\"'+newFields.ValidationFormat+'\", \n \"Errormessage\":\"'+newFields.Errormessage+'\" }';
         }
       }
       if(true){
        
        if(custom == 1){
           myJson = myJson+' \n } \n';
         }else{
           myJson = myJson+'\n },\n';
       }
       }
     
     custom--; 
     }
     myJson = myJson+'] \n }, \n';
     custom = collectionActivityFields.size();
     myJson = myJson+'{ \n \"Name\":\"Collection\", \n \"Field\": [  \n';
     for(ActivitySorted collection : collectionActivityFields){
        if(true){
         if(collection.DataType == 'PICKLIST'){
          myJson = myJson+'{ \n \"FieldLabel\": \"'+collection.FieldLabel+'\",\n \"uniquename\":\"'+(collection.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+collection.DataType+'\", \n \"Value\": ['+collection.value+' ],\n \"Default\":\"'+collection.DefaultValue+'\", \n \"ReadOnly\":\"'+collection.ReadOnly+'\", \n \"Visible\":\"'+collection.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+collection.FieldToCompare+'\", \n \"Operator\":\"'+collection.Operator+'\",\n \"ValidationFormat\":\"'+collection.ValidationFormat+'\", \n \"Errormessage\":\"'+collection.Errormessage+'\" }';
         }
         else if(collection.DataType == 'IMAGE' || collection.DataType == 'DATE'){
         myJson = myJson+'{ \n \"FieldLabel\": \"'+collection.FieldLabel+'\",\n \"uniquename\":\"'+(collection.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+collection.DataType+'\", \n \"ReadOnly\":\"'+collection.ReadOnly+'\", \n \"Visible\":\"'+collection.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+collection.FieldToCompare+'\", \n \"Operator\":\"'+collection.Operator+'\",\n \"ValidationFormat\":\"'+collection.ValidationFormat+'\", \n \"Errormessage\":\"'+collection.Errormessage+'\" }';
         }
         else{
         myJson = myJson+'{ \n \"FieldLabel\": \"'+collection.FieldLabel+'\",\n \"uniquename\":\"'+(collection.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+collection.DataType+'\", \n \"ReadOnly\":\"'+collection.ReadOnly+'\", \n \"Visible\":\"'+collection.Visible+'\", \n \"length\":\"'+collection.length+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+collection.FieldToCompare+'\", \n \"Operator\":\"'+collection.Operator+'\",\n \"ValidationFormat\":\"'+collection.ValidationFormat+'\", \n \"Errormessage\":\"'+collection.Errormessage+'\" }';
         }
       }
       if(true){
        
        if(custom == 1){
           myJson = myJson+' \n } \n';
         }else{
           myJson = myJson+'\n },\n';
       }
       }
     
     custom--; 
        
     }
     myJson = myJson+'] \n }, \n';
     custom = verificationActivityFields.size();
     myJson = myJson+'{ \n \"Name\":\"Verification\", \n \"Field\": [  \n';
     for(ActivitySorted verification : verificationActivityFields){
        if(true){
         if(verification.DataType == 'PICKLIST'){
          myJson = myJson+'{ \n \"FieldLabel\": \"'+verification.FieldLabel+'\",\n \"uniquename\":\"'+(verification.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+verification.DataType+'\", \n \"Value\": ['+verification.value+' ],\n \"Default\":\"'+verification.DefaultValue+'\", \n \"ReadOnly\":\"'+verification.ReadOnly+'\", \n \"Visible\":\"'+verification.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+verification.FieldToCompare+'\", \n \"Operator\":\"'+verification.Operator+'\",\n \"ValidationFormat\":\"'+verification.ValidationFormat+'\", \n \"Errormessage\":\"'+verification.Errormessage+'\" }';
         }
         else if(verification.DataType == 'IMAGE' || verification.DataType == 'DATE'){
         myJson = myJson+'{ \n \"FieldLabel\": \"'+verification.FieldLabel+'\",\n \"uniquename\":\"'+(verification.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+verification.DataType+'\", \n \"ReadOnly\":\"'+verification.ReadOnly+'\", \n \"Visible\":\"'+verification.Visible+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+verification.FieldToCompare+'\", \n \"Operator\":\"'+verification.Operator+'\",\n \"ValidationFormat\":\"'+verification.ValidationFormat+'\", \n \"Errormessage\":\"'+verification.Errormessage+'\" }';
         }
         else{
         myJson = myJson+'{ \n \"FieldLabel\": \"'+verification.FieldLabel+'\",\n \"uniquename\":\"'+(verification.uniquename).split('__c')[0]+'\", \n \"DataType\":\"'+verification.DataType+'\", \n \"ReadOnly\":\"'+verification.ReadOnly+'\", \n \"Visible\":\"'+verification.Visible+'\", \n \"length\":\"'+verification.length+'\", \n \"validation\": { \n \"FieldToCompare\":\"'+verification.FieldToCompare+'\", \n \"Operator\":\"'+verification.Operator+'\",\n \"ValidationFormat\":\"'+verification.ValidationFormat+'\", \n \"Errormessage\":\"'+verification.Errormessage+'\" }';
         }
       }
       if(true){
        
        if(custom == 1){
           myJson = myJson+' \n } \n';
         }else{
           myJson = myJson+'\n },\n';
       }
       }
     
     custom--; 
        
     }
     myJson = myJson+'] \n } \n ] \n } \n } \n }';
    // myJson= myJson+'] \n } \n } \n } ';
   // system.assertEquals(myJson,'abc');
    
    }

    public PageReference saveJson() {
    
        Document d = new Document();
        d.Name = 'text Config File';
        //String myContent = 'aabbcc';
        d.FolderId = '00lE0000000FPcZ';
        d.Body = Blob.valueOf(myJson);
        d.ContentType = 'JSON';
        d.Type = 'json';
        insert d;
        return null;
    }

   
   public String getCongifJson(){
   
   return myJson;
   } 

   
    
    
   

}