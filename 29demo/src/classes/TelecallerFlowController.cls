public class TelecallerFlowController{

    

    
    private Lead__c leadRecord;
    private Service__c serviceRecord;
    public String activityType { get;set;}
    public String appointmentDate{ get;set;}
    public String activityNotes {get;set;}
    public String userActivity{get;set{ userActivity = value; }}
        
    public TelecallerFlowController() {
        String currentLead = ApexPages.currentPage().getParameters().get('id');
        //String currentService = ApexPages.currentPage().getParameters().get('serviceId');
        if(currentLead != null && currentLead != ''){
            leadRecord = [select Id,Name,Lead_Status__c,Appointment_date__c,Preferred_Appointment_Time__c,ProductsOfInterest__c from Lead__c where Id = :currentLead];
            }
       /* if(currentService != null && currentService != ''){
            serviceRecord = [select Id,Lead__r.Name,Name,LeadStatus__c,Appointment_date__c from Service__c where Id = :currentService];
        } */
        //this.leadRecord = (Lead__c)controller.getRecord();
        soql = 'select Id,Name,Active__c,Required_Product_Doc__c,Required_Doc_Income_Proof__c,Required_Doc_Identity_proof__c,Optional_Product_doc__c,Optional_Doc_Income_Proof__c,Optional_Doc_Identity_Proof__c from Product__c  where Active__c = true ';
        runQuery();
        disableButton = true;
    }
    
    public String getLeadName(){
    if( leadRecord != null){
        return leadRecord.Name;
      }
     else if(serviceRecord != null){
         return serviceRecord.Lead__r.Name;
     }
    return null;
    }
    
    Task completedtask;
    public PageReference disqualifyLead(){
          completedtask=new Task();
          completedtask.Status = 'Completed';
          completedtask.Description = activityNotes;
          completedtask.Priority ='Normal';
          if(leadRecord != null){
              completedtask.WhatId = leadRecord.Id;
              completedtask.Subject = 'Contacted  ' + leadRecord.Name + ' on ' + date.today().format();
              leadRecord.Lead_Status__c = 'Disqualified';
              update leadRecord;
          }
          else if(serviceRecord != null){
              completedtask.WhatId = serviceRecord.Id;
              completedtask.Subject = 'Contacted  ' + serviceRecord.Lead__r.Name + ' on ' + date.today().format();
              serviceRecord.LeadStatus__c = 'Disqualified';
              update leadRecord;
          }
        return null;
    }

    
    
    
    
    

    public String selectedTime {get; set{ selectedTime = value; }}
      
      
      public PageReference checkCondition(){
           
      if(userActivity == 'Contacted' || userActivity == 'Not Reachable'){
          leadRecord.Lead_Status__c = userActivity;
          database.update(leadRecord);
      }
      else if( userActivity == 'Make an Appointment'){
          return Page.TeleCallerFlow_Products;
      }   
          completedtask=new Task();
          completedtask.WhatId = leadRecord.Id;
          completedtask.Subject = 'Contacted ' + leadRecord.Name  + ' on ' + date.today().format();
          completedtask.Status = 'Completed';
          completedtask.Description = activityNotes;
          completedtask.Priority ='Normal';
          return null;
      }
      
   
    

    public PageReference finish() {
        PageReference pg = new PageReference('/'+Lead__c.SObjectType.getDescribe().getKeyPrefix());
        return pg;
    }
    
    public String dueDate { get; set; }
    Task taskToCreate ;
    public Task getTask(){
        taskToCreate = new Task();
        return taskToCreate;
    }
    public PageReference createActivity(){
        PageReference p=new PageReference('/'+Lead__c.SObjectType.getDescribe().getKeyPrefix());
        taskToCreate.WhatId = leadRecord.Id;
        taskToCreate.OwnerId = UserInfo.getUserId();
        taskToCreate.Subject = 'Follow-up for ' + leadRecord.Name;
        taskToCreate.Status = 'Follow up';
        taskToCreate.Description = activityNotes;
        taskToCreate.ActivityDate =Date.parse(dueDate);
        List<Task> tasksToInsert = new List<Task>();
        tasksToInsert.add(taskToCreate);
        if(completedtask != null){
            tasksToInsert.add(completedtask);
        }  
        database.insert(tasksToInsert);
        leadRecord.Lead_Status__c = userActivity;
        database.update(leadRecord);
        return p;
    }
    
    // the soql without the order and limit
  private String soql {get;set;}
  // the collection of products to display
  public List<categoryWrapper> productsList = new List<categoryWrapper>();
  public Boolean disableButton { get; set; }
  public List<categoryWrapper> getProductsList(){
    return productsList;
  }
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
 
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
  // runs the actual query
  public void runQuery() {
        productsList.clear();
    try {
     for(Product__c pr : Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20')){
      CategoryWrapper cw = new CategoryWrapper(pr);
         productsList.add(cw);
         }
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
    String productName = Apexpages.currentPage().getParameters().get('productName');
    soql = 'select Id,Name,Active__c,Required_Product_Doc__c,Required_Doc_Income_Proof__c,Required_Doc_Identity_proof__c,Optional_Product_doc__c,Optional_Doc_Income_Proof__c,Optional_Doc_Identity_Proof__c from Product__c where Active__c = true ';
    if (!productName.equals(''))
      soql += ' and Name LIKE \''+String.escapeSingleQuotes(productName)+'%\'';
    // run the query again
    runQuery();
    return null;
  }
 
    
    public PageReference goBackToMainPage() {
        return Page.TeleCallerFlow_Main;
    }
    
          
    
    // the unique set of products that were checked/selected.
    public Set<String> selectedProductsSet {
        get {
            if (selectedProductsSet == null) selectedProductsSet = new Set<String>();
            return selectedProductsSet;
        }
        set;
    }
    
    public PageReference addProducts() {
        disableButton = false;
        for (CategoryWrapper cw : productsList) {
            if (cw.checked && !(selectedProductsSet.contains(cw.prod.Id))){
                selectedProductsSet.add(cw.prod.Id);
                selectedCategories.add(new CategoryWrapper(cw.prod));
               
            } 
        }
        return null;
    }
    
    // the products that were checked/selected.
    public List<categoryWrapper> selectedCategories {
        get {
            if (selectedCategories == null) selectedCategories = new List<categoryWrapper>();
            return selectedCategories;
        }
        set;
    } 
    
    //remove products from list
    public PageReference remove() {
        String selectedProdId = Apexpages.currentPage().getParameters().get('remove');
        //system.assertEquals('id',prodSelected);
        selectedProductsSet.remove(selectedProdId );
        List<CategoryWrapper> tempList = new List<CategoryWrapper>();
        for(CategoryWrapper cw :selectedCategories){
            if(cw.prod.Id != selectedProdId){
                tempList.add(cw);
            }
        }
        selectedCategories = tempList ;
        return null;
    }
    
        // fired when the back button is clicked
    public PageReference backToProducts() {
        return Page.TeleCallerFlow_Products;
    } 
    
    public PageReference goToAppointments() {
        if(selectedCategories != null){
        String productsSelected = '';
            for(CategoryWrapper cw : selectedCategories){
                Service__c service = new Service__c();
                ServiceDetailsWrapper ser = new ServiceDetailsWrapper(service);
                productsSelected  = productsSelected  + cw.Prod.Name + '\n';
                ser.service.Name = cw.Prod.Name;
                listOfServiceDetails.add(new ServiceDetailsWrapper(ser.service));
            }
            if(leadRecord.ProductsOfInterest__c != null){
            leadRecord.ProductsOfInterest__c = leadRecord.ProductsOfInterest__c + '\n' + productsSelected;
            }
            else{
            leadRecord.ProductsOfInterest__c =  productsSelected;
            }
        }
        return Page.TeleCallerFlow_Appointment;
    } 
    
    public string selectedProductName {get;set;}
    
    public List<String> reqdDocsList = new List<String>();
    public List<String> optionalDocsList = new List<String>();
    public List<String> getOptionalDocs(){
    return optionalDocsList;
    }
    
    public class DocumentClass{
        public String reqdDoc {get;set;}
        public String optionalDoc {get;set;}
    }
    List<DocumentClass> documentsList = new List<DocumentClass>();
    
    public List<DocumentClass> getdocumentsList(){
        return documentsList;
    }
    
    public boolean renderDocumentsSection = false;
    
    public boolean getDocumentSectionRender(){
        return renderDocumentsSection;
    }
    
    public PageReference getDocuments(){
        String prodSelected = Apexpages.currentPage().getParameters().get('selectedProduct');
        optionalDocsList.clear();
        reqdDocsList.clear();
        documentsList.clear();
        renderDocumentsSection = true;
        try{
            Product__c prodRecord = [select Name,Required_Product_Doc__c,Required_Doc_Income_Proof__c,Required_Doc_Identity_proof__c,Optional_Product_doc__c,Optional_Doc_Income_Proof__c,Optional_Doc_Identity_Proof__c from Product__c where Id = :prodSelected];
            selectedProductName = prodRecord.Name;
            if(prodRecord.Required_Product_Doc__c != null){
                List<String> temp = prodRecord.Required_Product_Doc__c.split(';');
                reqdDocsList.addAll(temp);
            }
            if(prodRecord.Required_Doc_Income_Proof__c != null){
                List<String> temp = prodRecord.Required_Doc_Income_Proof__c.split(';');
                reqdDocsList.addAll(temp);
            }
            if(prodRecord.Required_Doc_Identity_proof__c != null){
                List<String> temp = prodRecord.Required_Doc_Identity_proof__c.split(';');
                reqdDocsList.addAll(temp);
            }
            if(prodRecord.Optional_Product_doc__c != null){
                List<String> temp = prodRecord.Optional_Product_doc__c.split(';');
                optionalDocsList.addAll(temp);
            }
            if(prodRecord.Optional_Doc_Identity_Proof__c != null){
                List<String> temp = prodRecord.Optional_Doc_Identity_Proof__c.split(';');
                optionalDocsList.addAll(temp);
            }
            if(prodRecord.Optional_Doc_Income_Proof__c != null){
                List<String> temp = prodRecord.Optional_Doc_Income_Proof__c.split(';');
                optionalDocsList.addAll(temp);
            }
            reqdDocsList.sort();
            optionalDocsList.sort();
            if(reqdDocsList.size() > optionalDocsList.size()){
                Integer counter1 = 0;
                for(counter1 = 0; counter1< optionalDocsList.size() ; counter1++){
                    DocumentClass dc = new DocumentClass();
                    dc.reqdDoc = reqdDocsList[counter1];
                    dc.optionalDoc = optionalDocsList[counter1];
                    documentsList.add(dc);
                }
                for(Integer counter2 = counter1 ; counter2 < reqdDocsList.size() ; counter2++){
                    DocumentClass dc = new DocumentClass();
                    dc.reqdDoc = reqdDocsList[counter2];
                    documentsList.add(dc);
                }
            }
            else{
                Integer counter1 = 0;
                for(counter1 = 0; counter1< reqdDocsList.size() ; counter1++){
                    DocumentClass dc = new DocumentClass();
                    dc.optionalDoc = optionalDocsList[counter1];
                    dc.reqdDoc = reqdDocsList[counter1];
                    documentsList.add(dc);
                }
                for(Integer counter2 = counter1; counter2 < optionalDocsList.size() ; counter2++){
                    DocumentClass dc = new DocumentClass();
                    dc.optionalDoc = optionalDocsList[counter2];
                    documentsList.add(dc);
                }
            }
            
        }
        catch(Exception e){
        
        }
        return null;
    }
    
      // vivek code 25/7 start
  
   public List<service__c> listOfService = new List<service__c>();
    public List<ServiceDetailsWrapper> listOfServiceDetails = new List<ServiceDetailsWrapper>();
  
   
    public List<ServiceDetailsWrapper> getServiceDetails(){   
     return listOfServiceDetails;
   } 
   
   class ServiceDetailsWrapper{
           public Service__c service { get;set;}
           public String appointmentDate { get;set;}
           
           public ServiceDetailsWrapper(service__c s){
                   service = s;
                   appointmentDate = null;
           }
   }
     // vivek code 25/7 end
     
    public PageReference confirmAppointment() {
       leadRecord.Lead_Status__c='Appointment made';
        update leadRecord;
         
          //document for the lead should be created in Scanned Documents object
          
          
          // vivek code
          if(listOfServiceDetails != null){
                  listOfService.clear();
                  Map<String, Schema.RecordTypeInfo> mapset = Service__c.SObjectType.getDescribe().getRecordTypeInfosByName();
               for(ServiceDetailsWrapper sd : listOfServiceDetails)
               {
                Service__c ser = new Service__c();
                ser = sd.service;
                ser.Lead__c = leadRecord.Id; 
                ser.RecordTypeId = mapset.get('Sales-Service Prior Appointment').getRecordTypeId();
                ser.Appointment_date__c = Date.parse(sd.appointmentDate);
                listOfService.add(ser);
                }
               database.insert(listOfService);
               
             } 
             if(selectedCategories != null){
            List<ScannedDocument__c> listToInsert = new List<ScannedDocument__c>();
            for(CategoryWrapper cw :selectedCategories){
                ScannedDocument__c reqdDocRecord = new ScannedDocument__c();
                ScannedDocument__c optionalDocRecord = new ScannedDocument__c();
                List<String> reqDoc = new List<String>();
                List<String> optDoc = new List<String>();
                String reqdDocString = '';
                String optionalDocString = '';
                if(cw.prod.Required_Doc_Income_Proof__c != null){
                    String[] temp = cw.prod.Required_Doc_Income_Proof__c.split(';');
                    reqDoc.addAll(temp); 
                   // reqdDocString = reqdDocString + temp + '\n';
                }
                if(cw.prod.Required_Doc_Identity_proof__c != null){
                    String[] temp2 = cw.prod.Required_Doc_Identity_proof__c.split(';');
                    reqDoc.addAll(temp2); 
                   // reqdDocString = reqdDocString + temp + '\n';
                    }
                if(cw.prod.Required_Product_Doc__c != null){
                    String[] temp3 = cw.prod.Required_Product_Doc__c.split(';');
                    reqDoc.addAll(temp3); 
                   // reqdDocString = reqdDocString + temp + '\n';
                    }
                if(cw.prod.Optional_Product_doc__c != null){
                    String[] temp4 = cw.prod.Optional_Product_doc__c.split(';');
                     optDoc.addAll(temp4); 
                   // optionalDocString = optionalDocString + temp + '\n';
                    }
                if(cw.prod.Optional_Doc_Income_Proof__c != null){
                    String[] temp5 = cw.prod.Optional_Doc_Income_Proof__c.split(';');
                   optDoc.addAll(temp5); 
                   // optionalDocString = optionalDocString + temp + '\n';
                    }
                if(cw.prod.Optional_Doc_Identity_Proof__c != null){
                    String[] temp6 = cw.prod.Optional_Doc_Identity_Proof__c.split(';');
                    optDoc.addAll(temp6); 
                    //optionalDocString = optionalDocString + temp + '\n';
                    }
                   for(String s:reqDoc)
                   { 
                   reqdDocRecord=new ScannedDocument__c();
                    reqdDocRecord.Document_Name__c = s;
                    reqdDocRecord.Mandatory__c = true;
                   /* for(Service__c ser:listOfService)
                    {
                     if(ser.Name == cw.prod.Name)
                     {
                     reqdDocRecord.Lead__c = ser.id;
                     }
                    } */
                    reqdDocRecord.Lead__c = leadRecord.Id;
                   // reqdDocRecord.Product__c = cw.prod.Id;
                    listToInsert.add(reqdDocRecord);
                }
                for(String s:optDoc)
                {
                optionalDocRecord=new ScannedDocument__c();
                //optionalDocRecord.Product__c = cw.prod.Id;
                optionalDocRecord.Document_Name__c = s;
                optionalDocRecord.Mandatory__c = false;
                /* for(Service__c ser:listOfService)
                    {
                     if(ser.Name == cw.prod.Name)
                     {
                     optionalDocRecord.Service__c = ser.id;
                     }
                    } */
                optionalDocRecord.Lead__c = leadRecord.Id;
                listToInsert.add(optionalDocRecord);
                }
            }  
              database.insert(listToInsert);
          }
          List<Task> newTask = new List<Task>();
          for(Service__c ser:listOfService){
           completedtask=new Task();
           completedtask.WhatId = leadRecord.Id;
           completedtask.Subject = 'Created Service ' + ser.Name;// + ' for ' + leadRecord.Name + 'Appointment Date '+ser.Appointment_date__c;
           completedtask.Status = 'Completed';
           completedtask.Description = activityNotes;
           completedtask.Priority='Normal';
           newTask.add(completedtask);
          
          }
          database.insert(newTask);
          // vivek code end        
        PageReference pg = new PageReference('/apex/TeleCallerFlow_Confirmed?id='+leadRecord.Id);
        renderFirstBlock  = 'false';
        return null;
    }
    
     public String renderFirstBlock { get; set; }
     
    public List<Service__c> getCreatedServices(){
        return listOfService;
    }


    public PageReference backToProductResults() {
        return Page.TeleCallerFlow_Products;
    }
    
    public PageReference redirectToAssignRepsPage() {
    String serviceId = ApexPages.currentPage().getParameters().get('serviceId');
    PageReference pg = new PageReference('/apex/TeleCallerFlow_AssignReps?service_id='+serviceId);
    pg.setRedirect(true);
        return pg;
    }
    
 // assignSalesRep page   
Date selectedAppointmentDate;
String repZipCode,fromTimeInterval,toTimeInterval ;

public class FieldRepWrapper{
    public Sales_Area__c fieldRep{get;set;}
    public Boolean checked {get;set;}

    public FieldRepWrapper(Sales_Area__c fr){
        fieldRep = fr;
        checked = false;
    }
}
List<FieldRepWrapper> fieldRepWrapperList = new List<FieldRepWrapper>();
Service__c serviceToAssignRep ;
public List<FieldRepWrapper> getFieldReps(){
fieldRepWrapperList.clear();
String service_id = ApexPages.currentPage().getParameters().get('service_id');
if(service_id != null){
serviceToAssignRep = [select Appointment_date__c,Field_Rep__c,Lead__r.Zipcode__c from Service__c where Id = :service_id]; 
selectedAppointmentDate = serviceToAssignRep.Appointment_date__c;
repZipCode = serviceToAssignRep.Lead__r.Zipcode__c;
}
  for(Sales_area__c rep: [select Id,s.Field_Rep__r.Name,Field_Rep__c from Sales_area__c s where Field_Rep__c IN ( select s.Field_Rep__c From Service__c s where Appointment_Date__c = :selectedAppointmentDate ) and Zip_code__c = :repZipCode order by Name asc]){
      FieldRepWrapper wr = new FieldRepWrapper(rep);
      fieldRepWrapperList.add(wr);
  }
  return fieldRepWrapperList;
}

        public PageReference assignFieldRep(){
                String selectedRepId= '';
            for(FieldRepWrapper wrapper: fieldRepWrapperList){
                if(wrapper.checked == true){
                    selectedRepId = wrapper.fieldRep.Field_Rep__c;
                    break;
                }
            }
            if(selectedRepId != '' && serviceToAssignRep != null){
                //update lead with field Rep
                serviceToAssignRep.Field_Rep__c = selectedRepId;
                database.update(serviceToAssignRep);
                PageReference pg = new PageReference('/'+ serviceToAssignRep.Id);
                return pg;
            }
            else{
                //show apex error msg on VF page "Please select a field Rep before clicking on the button"
            }
            return null;
        }

 public PageReference skip() {
      String page = Apexpages.currentPage().getParameters().get('service_id');
      PageReference pg=new PageReference('/'+page);
        return pg;
    }
    
    public PageReference updateServices() {
        database.update(listOfService);
        return null;
    }
   
}