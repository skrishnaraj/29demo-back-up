public class MobileConsolePageDataInsertionController {

 List<Config__c> insertedConfig =new List<Config__c>();
 List<GeneralConfig__c> insertedGenConfig= new List<GeneralConfig__c>();
 List<Activity__c> insertedActivity = new List<Activity__c>(); 
 


    public PageReference selectFields() {
    //AllFields();wrapperCollection.clear();
        return new PageReference('/apex/MobileConsoleDataInsertion2');
    }


  public String RecordType { get; set; }

    public PageReference Next() {
    showSecondSection = true;
    showFirstSection  = false;
        return null;
    }
 public MobileConsolePageDataInsertionController()
 {
 showFirstSection = true;
 }
    
    
    public Boolean showSecondSection{get; set;}
    public Boolean showFirstSection{get; set;}
  public GeneralConfig__c genralConfig = new GeneralConfig__c();
  
  public GeneralConfig__c getGenConfig(){
  
  return genralConfig;
  }
     public Config__c getConfigValue() {
        return config;
    }

  public Config__c config = new Config__c();
  
  public Config__c getConfig(){
  
  return config;
  }
  
  // Second Page code
  
  class FieldWrapper{
        public String fieldLabel {get;set;}
        public String uniqueName {get;set;}
        public String dataType {get;set;}
        public Boolean readOnly {get;set;}
        public Boolean visible{get;set;}
        public Boolean disabled{get;set;}
        public Double length{get;set;}
        public Boolean sendToMobile{get;set;}
        public String PicklistValues{get;set;}
        public String DefaultPicklistValues{get;set;}
        
        public FieldWrapper(String label, String name, Integer length,Boolean disabled,String dataType ,String pickVal,String defaultVal){
            this.fieldLabel = label;
            this.uniqueName = name;
            this.readOnly = false;
            this.visible = false;
            this.length = length;
            this.disabled= disabled;
            this.sendToMobile= false;
            this.dataType = dataType;
            this.PicklistValues = pickVal;
            this.DefaultPicklistValues = defaultVal;
        }
    }
    
    public List<FieldWrapper> wrapperCollection = new List<FieldWrapper>();
    
    public List<FieldWrapper> getAllFields() {
        String type='Lead__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
         //Map<String, Schema.SObjectField> fieldMap2 = leadSchema.getDescribe();
        
        for (String fieldName: fieldMap.keySet()) {
        FieldWrapper fieldWrapperRow;
          String DataType = String.valueOf(fieldMap.get(fieldName).getDescribe().getType());
          if(DataType == 'REFERENCE'){
          fieldWrapperRow = new FieldWrapper(fieldMap.get(fieldName).getDescribe().getLabel(),'ID',18,!(fieldMap.get(fieldName).getDescribe().isUpdateable()),'ID',null,null);
          }else if(DataType == 'DOUBLE'){
           fieldWrapperRow = new FieldWrapper(fieldMap.get(fieldName).getDescribe().getLabel(),fieldName,50,!(fieldMap.get(fieldName).getDescribe().isUpdateable()),'STRING',null,null);
          }else if(DataType == 'PICKLIST' || DataType == 'MULTIPICKLIST'){
          Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
          List<Schema.PicklistEntry> pickVal = fieldResult.getPicklistValues();
          String allvalues = '';
          for(Schema.PicklistEntry p : pickVal){
          allvalues = allvalues+'\"'+p.getValue()+'\",';
          }
          Integer last = allvalues.lastIndexOfIgnoreCase(','); 
          //allvalues  = allvalues+'Hello';
          allvalues = allvalues.substring(0,allvalues.length()-1);
          
          fieldWrapperRow = new FieldWrapper(fieldMap.get(fieldName).getDescribe().getLabel(),fieldName,null,!(fieldMap.get(fieldName).getDescribe().isUpdateable()),'PICKLIST',allvalues,pickVal[0].getValue());
          
          }else if(DataType == 'DATE')
          {
          fieldWrapperRow = new FieldWrapper(fieldMap.get(fieldName).getDescribe().getLabel(),fieldName,null,!(fieldMap.get(fieldName).getDescribe().isUpdateable()),'DATE',null,null);
          }else{
          fieldWrapperRow = new FieldWrapper(fieldMap.get(fieldName).getDescribe().getLabel(),fieldName,fieldMap.get(fieldName).getDescribe().getLength(),!(fieldMap.get(fieldName).getDescribe().isUpdateable()),DataType,null,null);
          }
          
          
          
            //fieldWrapperRow = new FieldWrapper(fieldMap.get(fieldName).getDescribe().getLabel(),fieldName,false,false,false,!(fieldMap.get(fieldName).getDescribe().isUpdateable()),String.valueOf(fieldMap.get(fieldName).getDescribe().getSOAPType()));
            wrapperCollection.add(fieldWrapperRow);
        }
       // system.assertEquals(wrapperCollection,null);
        return wrapperCollection;
    }
    
     public PageReference navigateToThirdPage() {
     if(config.Id == null){
       Database.saveresult result = database.insert(config);
       if(result.isSuccess()){
       genralConfig.Config__c = result.getId();
       database.insert(genralConfig);
       List<Activity__c> toInsert = new List<Activity__c>();
       //system.assertEquals(wrapperCollection,null);
       for(FieldWrapper frp:wrapperCollection)
       {
       if(frp.sendToMobile == true){
       Activity__c newActivity = new Activity__c();
       newActivity.Config__c = result.getId();
       newActivity.Name = frp.fieldLabel ;
       newActivity.DataType__c = frp.dataType;
       
       newActivity.length__c = Double.valueOf(frp.length);
       newActivity.ReadOnly__c = frp.readOnly ;
       newActivity.Record_Type__c = RecordType;
       newActivity.uniquename__c = frp.uniqueName;
       newActivity.Visible__c = frp.visible;
       if(frp.dataType == 'PICKLIST'){
       newActivity.Defulat_Picklist_Value__c = frp.DefaultPicklistValues;
      // system.assertEquals(frp,null);
       /*String allvalues;
       for(Schema.PicklistEntry p: frp.PicklistValues){
       if(p != null){
       allvalues = allvalues+'\"'+p.getValue()+'\",';
       }
       
       } */
        newActivity.Picklist_Values__c = frp.PicklistValues; 
       //newActivity.Picklist_Values__c = '\"'+frp.PicklistValues[0].getValue()+'\",\"'+frp.PicklistValues[1].getValue();
       }
       
       toInsert.add(newActivity);
       }
       }
       //system.assertEquals(toInsert,null);
       database.insert(toInsert);
       insertedConfig = [select id,Name from Config__c where id=:result.getId()];
       }
       }else{
       List<Activity__c> toInsert = new List<Activity__c>();
       //system.assertEquals(wrapperCollection,null);
       for(FieldWrapper frp:wrapperCollection)
       {
       if(frp.sendToMobile == true){
       Activity__c newActivity = new Activity__c();
       newActivity.Config__c = config.Id;
       newActivity.Name = frp.fieldLabel ;
       newActivity.DataType__c = frp.dataType;
       
       newActivity.length__c = Double.valueOf(frp.length);
       newActivity.ReadOnly__c = frp.readOnly ;
       newActivity.Record_Type__c = RecordType;
       newActivity.uniquename__c = frp.uniqueName;
       newActivity.Visible__c = frp.visible;
       if(frp.dataType == 'PICKLIST'){
       newActivity.Defulat_Picklist_Value__c = frp.DefaultPicklistValues;
      // system.assertEquals(frp,null);
       /*String allvalues;
       for(Schema.PicklistEntry p: frp.PicklistValues){
       if(p != null){
       allvalues = allvalues+'\"'+p.getValue()+'\",';
       }
       
       } */
        newActivity.Picklist_Values__c = frp.PicklistValues; 
       //newActivity.Picklist_Values__c = '\"'+frp.PicklistValues[0].getValue()+'\",\"'+frp.PicklistValues[1].getValue();
       }
       
       toInsert.add(newActivity);
       }
       }
       //system.assertEquals(toInsert,null);
       database.insert(toInsert);
       
       insertedConfig = [select id,Name from Config__c where id=:config.Id];
       
       }
       
       
       wrapperCollection.clear();
        return new PageReference('/apex/MobileConsoleDataInsertion3');
     }
    
    List<String> datatypes = new List<String>();
    
    // Third Page Code
    
    
   
    
     public List<GeneralConfig__c> getGenConfig2(){
     return [select id,name,config__c from GeneralConfig__c where config__c IN:insertedConfig];
     }
 
     public List<Activity__c> getActivity(){
     return [select id,name,config__c from Activity__c where config__c IN:insertedConfig];
     }
    
}