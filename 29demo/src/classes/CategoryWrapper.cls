public class CategoryWrapper {
 
    public Boolean checked{ get; set; }
    public Product__c prod { get; set;}
 
    public CategoryWrapper(Product__c c){
        prod = c;
        checked = false;
    }
    
    public static testMethod void testMe() { 
 
        CategoryWrapper cw2 = new CategoryWrapper(new Product__c(name='Test1'));
        System.assertEquals(cw2.prod.name,'Test1');
        System.assertEquals(cw2.checked,false);       
 
    }
 
}