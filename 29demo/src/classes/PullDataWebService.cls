@RestResource(urlMapping='/PullDataFromServer1/*')
global class PullDataWebService extends LeadRecords{  

global class MobileData{
    public String Time_Stamp {get;set;}
    public List<PullDataWebService> records{get;set;}
}
   
   @HttpGet
   global static MobileData getRecords() {
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   String appDateString = req.params.get('Appointment_Date');
   String IMEI_Number= req.params.get('IMEI_Number');
   
   String timeStamp = req.params.get('Time_Stamp');
   String tempDateTime;
   if(timeStamp != null){
    tempDateTime = Date.today().format() + '' + timeStamp.substring(timeStamp.length()-9, timeStamp.length());
   }
   DateTime timeStampDate;
   Date appointmentDate;
   try{
    //Date t = ;
    if(dateTime.now() >dateTime.parse(date.today().format()+ ' 7:59 PM') || dateTime.now() < dateTime.parse(date.today().format()+ ' 9:00 AM')  ){
            res.addHeader('Custom_Status_Code', '407');
            return null;
        }
    if(timeStamp != null){
        timeStampDate = dateTime.parse(tempDateTime);
    }
    //appointmentDate=date.parse(appDateString);
    appointmentDate=date.today();
   }
   catch(Exception ex){
        res.addHeader('Custom_Status_Code', '401');
        return null;
   } 
   
   
   List<PullDataWebService> leadRecordList= new List<PullDataWebService>();
   List<Lead__c> resultsList = new List<Lead__c>();
    String query ;
    String fieldRepId;
    try{
        fieldRepId = [select Id from FieldRep__c where IMEI_Number__c = :IMEI_Number and Active__c = true].Id;
    }
    catch(Exception e){
        if(e.getMessage().contains('List has no rows for assignment')){
            res.addHeader('Custom_Status_Code', '400');
            return null;
        }
     }
    if(timeStamp == null || timeStamp == ''){
        resultsList = [SELECT Lead_Name__c,Appointment_Date_Time__c,Phone__c,Home_Country_Address__c, Occupation__c,SR_Appointment_Status__c, Id,SR_Appointment_Comment__c,ProductsOfInterest__c,RecordType.Name,(Select Id,Name,Document_Name__c,Number_Of_Pages__c,Mandatory__c From ScannedDocuments__r) FROM Lead__c WHERE Field_Rep__c = :fieldRepId and Appointment_date__c = :appointmentDate and Lead_Status__c NOT IN ('Sold','Disqualified','Cancelled','Cancelled @ door')];
    }
    else{
        resultsList =[select Lead_Name__c,Appointment_Date_Time__c,Phone__c,Home_Country_Address__c, Occupation__c,SR_Appointment_Status__c, Id,SR_Appointment_Comment__c,ProductsOfInterest__c,RecordType.Name,(Select Id,Name,Document_Name__c,Number_Of_Pages__c,Mandatory__c From ScannedDocuments__r) FROM Lead__c WHERE Field_Rep__c = :fieldRepId and Appointment_date__c = :appointmentDate and LastModifiedDate >= :timeStampDate and Lead_Status__c NOT IN ('Sold','Disqualified','Cancelled','Cancelled @ door')];
    }
   for(Lead__c l: resultsList)
   {
    List<LeadDocument> tempStorage = new List<LeadDocument>(); 
    for(Integer i = 0; i< l.ScannedDocuments__r.size();i++){
        LeadDocument d = new LeadDocument();
        d.Mandatory = l.ScannedDocuments__r[i].Mandatory__c;
        d.numberOfImages = l.ScannedDocuments__r[i].Number_Of_Pages__c;
        d.Name = l.ScannedDocuments__r[i].Document_Name__c;
        tempStorage.add(d);
    }
    if(l.RecordType.Name == 'Verification'){
        PullDataWebService lr=new PullDataWebService();
        lr.Id=l.Id;
        lr.SR_Appointment_Status = l.SR_Appointment_Status__c;
        lr.SR_Appointment_Comment = l.SR_Appointment_Comment__c;
        //lr.Date_of_Birth = l.Date_Of_Birth__c.format();
        lr.ActivityName = l.RecordType.Name;
        leadRecordList.add(lr);
        
    }
    else if(l.RecordType.Name == 'Collection'){
        PullDataWebService lr = new PullDataWebService();
        lr.Id=l.Id;
        lr.Name=l.Lead_Name__c;
        lr.SR_Appointment_Status = l.SR_Appointment_Status__c;
        lr.SR_Appointment_Comment = l.SR_Appointment_Comment__c;
        //lr.Appointment_Comment = l.Appointment_Comments__c;
        //lr.Sharing_Reason = l.Sharing_Reason__c;
        String addr = '';
        if(l.Home_Country_Address__c != null){
            addr = l.Home_Country_Address__c;
         }
         if(l.Home_Country_District__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.Home_Country_District__c :l.Home_Country_District__c;
         }
         if(l.Home_Country_State__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.Home_Country_State__c :l.Home_Country_State__c;
         }
         if(l.Zipcode__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.Zipcode__c :l.Zipcode__c;
         }
        lr.Home_Country_Address = addr;
        //lr.Sharing_Reason = l.Sharing_Reason__c;
        lr.ActivityName = l.RecordType.Name;
        ScannedDocument__c dummyDocument = new ScannedDocument__c();
        dummyDocument.Document_Name__c = 'Collection Document';
        dummyDocument.Number_Of_Pages__c = 1;
        dummyDocument.Lead__c = l.Id;
        List<ScannedDocument__c> tempList1 =[select Id,Name from ScannedDocument__c where Lead__c =:l.Id];
        if(tempList1.size() == 0){
            database.insert(dummyDocument);
        }
        LeadDocument d = new LeadDocument();
        d.Mandatory = true;
        d.numberOfImages = dummyDocument.Number_Of_Pages__c;
        d.Name = dummyDocument.Document_Name__c ;
        List<LeadDocument> test = new List<LeadDocument>{d};
        lr.documentsList = test;
        leadRecordList.add(lr);
    }
    else{
         PullDataWebService lr=new PullDataWebService();
         lr.Name=l.Lead_Name__c;
         String addr ='';
         /*if(l.Street__c != null){
            addr = l.Street__c;
         }
         if(l.City__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.City__c :l.City__c;
         }
         if(l.State_Province__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.State_Province__c :l.State_Province__c;
         }
         if(l.Zipcode__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.Zipcode__c :l.Zipcode__c;
         }
         if(l.Country__c != null){
            addr = (addr.length()>0)? addr + ' , ' + l.Country__c :l.Country__c;
         }
         lr.Address= addr;
         */
         lr.ProductsOfInterest=l.ProductsOfInterest__c;
         lr.Id=l.Id;
         //lr.Appointment_Date_Time=l.Appointment_Date__c.format()+' '+l.Preferred_Appointment_Time__c;
         lr.Phone = l.Phone__c;
         lr.Occupation = l.Occupation__c;
         lr.SR_Appointment_Status = l.SR_Appointment_Status__c;
         lr.SR_Appointment_Comment = l.SR_Appointment_Comment__c;
         lr.ActivityName = l.RecordType.Name;
         if(tempStorage.size() > 0){
            lr.documentsList = tempStorage;
         }
         leadRecordList.add(lr);
    }
   }
   MobileData md = new MobileData();
   md.records = leadRecordList;
   md.Time_Stamp = datetime.now().format();
   system.debug(md + 'Mobile data');
   if(leadRecordList.size() == 0){
        res.addHeader('Custom_Status_Code', '203');
   }
   else{
        res.addHeader('Custom_Status_Code', '204');
   }
   return md;
 }
}