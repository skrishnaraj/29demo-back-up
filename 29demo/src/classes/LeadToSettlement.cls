public class LeadToSettlement {


    public PageReference EditRecords() { 
        DisplayNonEditable = !(DisplayNonEditable );        
        return null;
    }

    public Boolean renderTransactionTable { get; set; }
    
    
     public Boolean DisplayNonEditable = true;
    public Boolean getdisplayNonEditable(){
        return DisplayNonEditable;
    }
    
    public Boolean DisplayEditButton = true;
    public Boolean getDisplayEditButton(){
        return DisplayEditButton;
    }

    public String validity { get; set; }
    public String settlementCase { get; set; }
    Lead__c leadDetails = new Lead__c();
    public String billingDate { get; set; }
    public String OtherValuesDays { get; set; }
    public String selectPeriod { get; set; }
    public String selectOpt { get; set; }
    public Boolean otherValue = false;
    
     public Boolean getOtherValue(){
        return otherValue;
    }
        
     public Boolean transactionTable =false;
      public Boolean getTransactionTable(){
        return transactionTable;
    }
    public Boolean serviceIndividual = false;
     public Boolean getServiceIndividual(){
        return serviceIndividual;
    }
     public Boolean settlementSection= false;
    
     public Boolean getSettlementSection(){
        return settlementSection;
    }
    
     public Boolean settlementAmount = true;
    
     public Boolean getSettlementAmount(){
        return settlementAmount;
    }
    
    public PageReference serviceSettlementFinal() {
         Double outstandingAmount = 0.0;
         Double settlementAmountTotal = 0.0;
         Double principalOutstandingN = 0.0; 
         listOfServicesToSettle.clear();
         tvInstance =  new TotalValueClass();
         
         for (ServiceDetails cw : listOfserviceDetails) {
            if (cw.checked){
            
                listOfServicesToSettle.add(cw.serviceRecord);
                if(cw.serviceRecord.Outstanding_Amount__c != null){
                  outstandingAmount = outstandingAmount + cw.serviceRecord.Outstanding_Amount__c;
                   }
                   if(cw.serviceRecord.Principal_Outstanding__c != null){
                  principalOutstandingN = principalOutstandingN + cw.serviceRecord.Principal_Outstanding__c;
                  }
               if(cw.serviceRecord.Settlement_Amount__c != null){
              
               settlementAmountTotal = settlementAmountTotal + cw.serviceRecord.Settlement_Amount__c;}
               
            } 
        }
        
        tvInstance.totalOutstanding = Decimal.ValueOf(outstandingAmount).setScale(2);   
        tvInstance.totalSettlement =  Decimal.ValueOf(settlementAmountTotal).setScale(2) ; 
        tvInstance.principalOutstanding = Decimal.ValueOf(principalOutstandingN).setScale(2) ;
        Boolean sayTrue = true;
        //system.assertEquals(listOfServicesToSettle,null);
        for(service__c ser : listOfServicesToSettle){
        
        if(ser.Settlement_Amount__c > ser.Outstanding_Amount__c){
        //system.assertEquals(ser.Outstanding_Amount__c,null);
        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Settelement Amount Should Not be Greater Than Outstabding Amount');
        sayTrue = false;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Settelement Amount Should Not be Greater Than Outstabding Amount'));
        }
        
        }    
        
        if(sayTrue){
        settlementAmount = false;
        firstSection = true ;
        }
        return null;
    }

    List<Service__c> listOfServicesToSettle = new List<Service__c>();
    public PageReference serviceSettlement() {
        listOfServicesToSettle.clear();
        for (ServiceDetails cw : listOfserviceDetails) {
            if (cw.checked){
                listOfServicesToSettle.add(cw.serviceRecord); 
            } 
        }
        return null;
    }
    
    public List<Service__c> getServSett(){   
        return listOfServicesToSettle;
    }

    
    public PageReference installmentPeriod() {      
    otherValue = false; 
    if(selectPeriod == 'Other'){
        otherValue = true;
    }
    else{otherValue = false; }  
        return null;
    }
  
    
        public TotalValueClass tvInstance;
         public PageReference createTransaction() {
         transactionTable = false;
         settlementAmount = true;
         firstSection  = false;
         Double outstandingAmount = 0.0;
         Double settlementAmount = 0.0;
         Double principalOutstandingN  = 0.0;
         serviceIndividual = true ;

         listOfServicesToSettle.clear();
         tvInstance =  new TotalValueClass();
         
         for (ServiceDetails cw : listOfserviceDetails) {
            if (cw.checked){
                listOfServicesToSettle.add(cw.serviceRecord);
                 if(cw.serviceRecord.Outstanding_Amount__c != null){
                  outstandingAmount = outstandingAmount + cw.serviceRecord.Outstanding_Amount__c;
                   }
                   if(cw.serviceRecord.Principal_Outstanding__c != null){
                  principalOutstandingN = principalOutstandingN + cw.serviceRecord.Principal_Outstanding__c;
                  }
               if(cw.serviceRecord.Settlement_Amount__c != null){
              
               settlementAmount = settlementAmount + cw.serviceRecord.Settlement_Amount__c;}
               
            } 
        }   
        tvInstance.totalOutstanding = Decimal.ValueOf(outstandingAmount).setScale(2);   
        tvInstance.totalSettlement =  Decimal.ValueOf(settlementAmount).setScale(2) ;   
        tvInstance.principalOutstanding = Decimal.ValueOf(principalOutstandingN).setScale(2) ;   
        return null;
    }
     
   public TotalValueClass getTotalAmount(){
        return tvInstance; 
   } 
    
    public Boolean tableOfServ = false;
    
    public Boolean getTableOfServ(){
        return tableOfServ;    
    }
    
     public Boolean firstSection = false;
    
    public Boolean getFirstSection(){
        return firstSection;    
    }

    public List<ServiceDetails> listOfserviceDetails = new List<ServiceDetails>();

    
    class ServiceDetails{
        public Boolean checked { get; set; }
        public Service__c serviceRecord { get; set; }
    
        public ServiceDetails(Service__c s){
            serviceRecord = s;
            checked = false;
        }
    
    }
    
    public class TotalValueClass{
        public double totalOutstanding { get; set; }
        public double totalSettlement { get; set; }
        public double principalOutstanding { get; set; }
    }
    
    public List<ServiceDetails> getServiceNames(){
        return listOfserviceDetails;
    }
    
    String currentLead;
    
    public LeadToSettlement(){
        currentLead = ApexPages.currentPage().getParameters().get('id');
        leadDetails = [select id,Name,Total_Settlement_Amount__c,Total_Outstanding_Amount__c,Approval_Status__c,Settlement_Reason__c from Lead__c where id=:currentLead];
        for(Service__c s:[select id,Name,Lead__c,Outstanding_Amount__c,Principal_Outstanding__c,Settlement_Amount__c,card_loan_no__c from service__c where Lead__c=:currentLead]){
            ServiceDetails serDt = new ServiceDetails(s);
            listOfserviceDetails.add(serDt);
        }
    }
     
     List<Transaction__c> listOfTransactions = new List<Transaction__c>();
     //List<Transaction__c> finalListOfTransactions = new List<Transaction__c>();
     Map<Integer,List<Transaction__c>> finalListOfTransactions = new Map<Integer,List<Transaction__c>>();
     List<Task> tasksToBeCreated = new List<Task>();
      
     public PageReference calculateTransactions() {
        tableOfServ = false;
        settlementSection = false;
        firstSection = false;
        serviceIndividual = false;
       
        renderTransactionTable  = true;
        listOfTransactions.clear();

        if(selectOpt != '' || selectOpt != null){
            if(selectOpt == '0')
            {

            }
            else{
                Integer noOfInstalments =Integer.ValueOf(selectOpt);
                Integer loopCount = 1;
                for(Service__c serviceLoopVar : listOfServicesToSettle)
                {
                    Date billing_Date = Date.parse(billingDate);
                    Date validity_Date = Date.parse(validity);
                    Decimal installmentAmount = (serviceLoopVar.Settlement_Amount__c/noOfInstalments).setScale(2); 
                    listOfTransactions = new List<Transaction__c>();
                    for(Integer i=0;i< noOfInstalments;i++){
                        if(selectPeriod == 'Weekly'){
                            if(i==0){
                            createTasksAndTransactions(false,0,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            }
                            else{
                            
                            createTasksAndTransactions(false,7,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            billing_Date = billing_Date.addDays(7);
                            }
                        }
                        if(selectPeriod == 'BiWeekly'){ 
                            if(i==0){
                            createTasksAndTransactions(false,0,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            }
                            else{
                            createTasksAndTransactions(false,14,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            billing_Date = billing_Date.addDays(14);
                            }
                        }
                        if(selectPeriod == 'Monthly'){
                            if(i==0)
                            {
                            createTasksAndTransactions(true,0,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            }
                            else{
                            createTasksAndTransactions(true,1,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            billing_Date = billing_Date.addMonths(1);
                            }
                        }
                        if(selectPeriod == 'Quaterly'){
                            if(i==0){
                            createTasksAndTransactions(true,0,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            }
                            else{
                            createTasksAndTransactions(true,3,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            billing_Date = billing_Date.addMonths(3);
                            }
                        }
                        if(OtherValuesDays != '' && OtherValuesDays != null){
                            Integer noOfDays = Integer.ValueOf(OtherValuesDays);
                            if(i==0){
                            createTasksAndTransactions(false,0,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);
                            }
                            else{
                            createTasksAndTransactions(false,noOfDays,billing_Date,installmentAmount,serviceLoopVar.Id,validity_Date,currentLead,'Transaction outstandingAmount due tommorow for '+serviceLoopVar.Name,'Follow Up',serviceLoopVar.Settlement_Amount__c);    
                            billing_Date = billing_Date.addDays(noOfDays);
                            }
                        }
                    } 
                    billing_Date = null;
                    finalListOfTransactions.put(loopCount++,listOfTransactions);
                }
            }

        }
        //system.assertEquals(finalListOfTransactions,null);
        return null;
    }
    
    void createTasksAndTransactions(Boolean month,Integer noOfDaysOrMonths,Date billingDate,Decimal installmentAmount,String serviceId,Date validityDate,String leadId,String subject,String status,Decimal serviceAmount){
        Transaction__c tempTransaction = new Transaction__c();
        if(month == false){
            tempTransaction.Bill_Date__c = billingDate.addDays(noOfDaysOrMonths);
        }
        else{
            tempTransaction.Bill_Date__c = billingDate.addMonths(noOfDaysOrMonths);
            }
        tempTransaction.Amount_To_be_Paid__c = installmentAmount;
        tempTransaction.Service_Name__c = serviceId;
        tempTransaction.Validity_Date__c = validityDate;
        tempTransaction.Lead__c = leadId;
        tempTransaction.Service_Settlement_Amount__c = serviceAmount;
        listOfTransactions.add(tempTransaction);

        Task taskToInsert = new Task();
        taskToInsert.OwnerId = UserInfo.getUserId();
        taskToInsert.WhatId = leadId;
        taskToInsert.Subject= subject;
        taskToInsert.Status = status;
        if(month == false){
            taskToInsert.ActivityDate = billingDate.addDays(noOfDaysOrMonths);
        }
        else{
            taskToInsert.ActivityDate = billingDate.addMonths(noOfDaysOrMonths);
        }
        tasksToBeCreated.add(taskToInsert);
        //billing_Date = billingDate.addDays(noOfDays);
    }
    
    
    public PageReference insertTransactions() {
        if(finalListOfTransactions.size() > 0){
            database.update(listOfServicesToSettle);
            List<Transaction__c> recordsToInsert = new List<Transaction__c>();
            for(List<Transaction__c> subRecords : finalListOfTransactions.values()){
                recordsToInsert.addAll(subRecords);
            }
            database.insert(recordsToInsert);
            leadDetails.Approval_Status__c = 'Settlement Approval Pending';
            database.update(leadDetails);
        }
        return null;
    }
    
    public Map<Integer,List<Transaction__c>> getTransactionRecords1(){
        return finalListOfTransactions;
    }
    
     public PageReference displayEditedTransaction() {       
        DisplayNonEditable = true;
        return null;
    }
    
    public Lead__c getLeadDetail(){
        return leadDetails;
    }

}