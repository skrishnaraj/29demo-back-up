public class FieldRepMapController {

   String FieldRepNameT;
   String DateApp;

    public FieldRepMapController (){
        String FieldRepname = ApexPages.currentPage().getParameters().get('FieldRep');
        if(FieldRepname != null){
            String[] temp=FieldRepname.split(';');
            DateApp=temp[0];
            FieldRepNameT=temp[1];
        }
    }


    public List<List<String>> locations = new List<List<String>>();


  public List<List<String>> getLocations(){
   
  Date current = Date.parse(DateApp);
  
  for(Location_Data__c location: [Select id,Latitude__c,Longitude__c,FieldRep__r.Name,CapturedDateTime__c from Location_Data__c where FieldRep__r.Name=:FieldRepNameT and currentDate__c=:current order by CapturedDateTime__c]){
      List<String> temp = new List<String>();
      if(location.Latitude__c != '0' && location.Longitude__c != '0'){
      temp.add(location.Latitude__c);
      temp.add(location.Longitude__c);
      locations.add(temp);
      }
      
  }
  return locations;
  }
}