global class DistanceCalculator {
//String route = 'https://maps.googleapis.com/maps/api/directions/json?originA=Girinagar,Bangalore,India&destination=Electronics%20City,Bangalore,India&waypoints=Jayanagar,Bangalore,India|Banashankari,Bangalore,India&sensor=false';
    static String base_route = 'https://maps.googleapis.com/maps/api/directions/xml?origin=';

    class UserDetails{
        String fieldRepId;
        List<Location_Data__c> userTravelList = new List<Location_Data__c>();
    }

    @future(callout=true)
    public static void callGoogleAPI() {
        List<UserDetails> listOfUsers = new List<UserDetails>();
         List<Location_Data__c> newList = new List<Location_Data__c>();
        UserDetails currentUser ;
        for(Location_Data__c loc :[select Latitude__c,Longitude__c,FieldRep__c,Name,Processed__c from Location_data__c where Processed__c != true and Ignore_Record__c = false AND currentDate__c = :date.today() order by FieldRep__c,CapturedDateTime__c asc]){
            if(currentUser == null){
                currentUser = new UserDetails();
                currentUser.fieldRepId = loc.FieldRep__c;
            }
            if(currentUser.fieldRepId != loc.FieldRep__c){
                listOfUsers.add(currentUser);     
                currentUser = new UserDetails();
                currentUser.userTravelList.add(loc);
                currentUser.fieldRepId = loc.FieldRep__c;
            }
            else if(currentUser.fieldRepId == loc.FieldRep__c){
                currentUser.userTravelList.add(loc);
            }
        }
        listOfUsers.add(currentUser);
        List<Travel_Data__c> recordsToInsert = new List<Travel_Data__c>();
        List<Travel_Data__c> recordsToUpdate = new List<Travel_Data__c>();
        List<Location_data__c> dataList = new List<Location_data__c>();
        Travel_Data__c td = new Travel_Data__c();
        Integer totalCallouts = 0;
        for(UserDetails u :listOfUsers){
            String origin;
            String route = '';
            String destination;
            String wayPoints = '';
            Integer totalDistance = 0;
            Double distanceInKm = 0.0;
            Integer hopCount = 0;
            Integer distancesToCalculate = 0;
            
            dataList.clear();
            dataList = u.userTravelList;
            if(totalCallouts > 9){
                break;
            }
            if(dataList.size() <2){
                continue;
            }
            if(dataList.size()> 2 && dataList.size() <11){
                //waypoints exist
                origin = dataList[0].Latitude__c + ',' + dataList[0].Longitude__c;
                dataList[0].Processed__c = true;
                distancesToCalculate = 1;
                for(Integer x = 1; x<dataList.size()-1; x++){
                    wayPoints += dataList[x].Latitude__c + ',' + dataList[x].Longitude__c + '|';
                    dataList[x].Processed__c = true;
                    distancesToCalculate++;
                }
                destination = dataList[dataList.size() - 1].Latitude__c + ',' + dataList[dataList.size() - 1].Longitude__c;
                route = base_route + origin + '&destination=' + destination + '&waypoints=' + wayPoints +'&sensor=false';
                // increase callout count
                totalCallouts++;
            }
            else if(dataList.size() > 10){
                //waypoints exist
                origin = dataList[0].Latitude__c + ',' + dataList[0].Longitude__c;
                dataList[0].Processed__c = true;
                distancesToCalculate = 1;
                for(Integer x = 1; x<9; x++){
                    wayPoints += dataList[x].Latitude__c + ',' + dataList[x].Longitude__c + '|';
                    dataList[x].Processed__c = true;
                    distancesToCalculate++;
                }
                destination = dataList[9].Latitude__c + ',' + dataList[9].Longitude__c;
                route = base_route + origin + '&destination=' + destination + '&waypoints=' + wayPoints +'&sensor=false';
                // increase callout count
                totalCallouts++;
                
            }
            else if(dataList.size() >0 && dataList.size() == 2){
                //only origin and destination exist
                origin = dataList[0].Latitude__c + ',' + dataList[0].Longitude__c;
                dataList[0].Processed__c = true;
                destination = dataList[1].Latitude__c + ',' + dataList[1].Longitude__c;
                route = base_route + origin + '&destination=' + destination + '&sensor=false';
                distancesToCalculate = 1;
                // increase callout count
                totalCallouts++;
            }


            // Create HTTP request to send. 
            HttpRequest request = new HttpRequest();
            request.setEndPoint(route);
            system.debug(route + 'ROUTE');
            request.setMethod('GET');
            request.setTimeout(120000);
            //system.assertEquals(request,null);
            // Send the HTTP request and get the response. 
            Http http = new Http();
            // The response is in JSON format. 
            HttpResponse response = http.send(request);
            //system.assertEquals(response.getBody(),null);
            
            List<String> components = new List<String>();
            dom.Document resDoc = response.getBodyDocument();
            Dom.XmlNode rootElement = resDoc.getRootElement();
            Dom.XmlNode route1= rootElement.getChildElement('route',null);
            if(route1 == null){
                continue;
            }
            Dom.XmlNode[] legs = route1.getChildElements();
            for(Dom.XMLNode child : route1.getChildElements()){
                if(child.getName() == 'leg'){
                    //system.debug(child.getChildElement('distance',null).getChildElement('value',null).getText() + ' dist calculations');
                    hopCount++;
                    Integer distance = Integer.valueOf(child.getChildElement('distance',null).getChildElement('value',null).getText()); 
                    totalDistance += distance;
                    if(hopCount == distancesToCalculate){
                        break;
                    }
                }
            }

            newList.addAll(dataList);
            //system.debug('New list '+newList);
            if(totalDistance!= 0){
                distanceInKm = (Double)totalDistance/1000;
                try{
                    td = [select distance__c,FieldRep__c,TravelDate__c from Travel_data__c where  TravelDate__c = :date.today() and FieldRep__c = :u.fieldRepId];
                        td.distance__c = td.distance__c + distanceInKm;
                        recordsToUpdate.add(td);
                    }
                catch(Exception e){
                    distanceInKm = (Double)totalDistance/1000;
                    td = new Travel_Data__c();
                    td.distance__c = distanceInKm;
                    td.FieldRep__c = u.fieldRepId;
                    td.TravelDate__c = date.today();//should be date.TODAY()
                    recordsToInsert.add(td);
                }
            }
       }
       if(newList.size()>0){
            Database.Saveresult[] results = database.update(newList,true);
         }
        if(recordsToInsert.size() > 0){
            database.insert(recordsToInsert);
        }
        if(recordsToUpdate.size() > 0){
            database.update(recordsToUpdate);
        }

    }
    

}