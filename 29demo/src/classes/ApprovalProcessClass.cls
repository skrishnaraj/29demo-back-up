global class ApprovalProcessClass {

    WebService static void updateApprovalStatus(string id) {
    Lead__c leadDetail = [select Id,Name,Approval_Status__c from Lead__c where id=:id];
    	leadDetail.Approval_Status__c = 'Field Approval Pending';
    	database.update(leadDetail);
    }

}