global class CreateQueuesForTeleCallers {
    
    WebService static void CreateQueues(List<String> teleCallerIds) {
        List<Group> groupsToInsert = new List<Group>();
        List<GroupMember> membersToInsert = new List<GroupMember>();
        List<QueueSObject> queuesToInsert = new List<QueueSObject>();
        Map<String,Telecaller__c> groupName_TeleCaller_Map = new Map<String,Telecaller__c>(); 
        Set<String> existingGroupNames = new Set<String>();
        for(Group existingRec : [select Name,Id from Group where Type = 'Queue']){
            existingGroupNames.add(existingRec.Name);
        }
        for(Telecaller__c teleCaller : [select Id,Name,Manager__c,Email__c from Telecaller__c where Id in :teleCallerIds]){
            if(!existingGroupNames.contains(teleCaller.Name)){
                Group newGroup = new Group(Name=teleCaller.Name, Type='QUEUE',email = teleCaller.Email__c);
                groupsToInsert.add(newGroup);
                groupName_TeleCaller_Map.put(newGroup.Name,teleCaller);
            }
        }
        if(groupsToInsert.size() > 0){
            database.insert(groupsToInsert);
        }
        for(Group insertedGroup :groupsToInsert){
            Telecaller__c user = groupName_TeleCaller_Map.get(insertedGroup.Name);
            GroupMember newGroupMember = new GroupMember(UserOrGroupId = user.Manager__c,GroupId = insertedGroup.Id);
            membersToInsert.add(newGroupMember);
            QueueSObject newQueue= new QueueSObject(queueid=insertedGroup.id, sobjectType='Lead__c');
            queuesToInsert.add(newQueue);
        }
        database.insert(membersToInsert);
        database.insert(queuesToInsert);
    }

}