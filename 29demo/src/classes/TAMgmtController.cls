public class TAMgmtController {

    public String strTAId {get;set;}

    public PageReference deleteTARecord() {
              TA__c acctDel = new TA__c(Id=strTAId);
                
                delete acctDel;
    
        return null;
    }


    public TAMgmtController() {

    }


    public TAMgmtController(ApexPages.StandardController controller) {

    }


        public PageReference enterRec() {
        PageReference pref=new PageReference('/apex/TAManagement');
        string Name=Apexpages.Currentpage().getParameters().get('Name');
        string frmDate=Apexpages.Currentpage().getParameters().get('frmDate');
        string toDate=Apexpages.Currentpage().getParameters().get('toDate');
        string taValue=Apexpages.Currentpage().getParameters().get('taValue');
        //system.assertEquals(Name,'adf');
        
        date mydate1;
         TA__c t=new TA__c();
        if(toDate != null && toDate != ''){
        mydate1 = date.parse(toDate);
        t.To_Date__c=mydate1;
        }
        date mydate = date.parse(frmDate);
        
        
       
        t.Name=Name;
        t.From_Date__c=mydate;
        
        t.TA_Value__c=Decimal.valueOf(taValue);
        TA__c last;
        try{
        last=[Select id,Name,To_Date__c,From_Date__c,TA_Value__c From TA__c ORDER BY From_Date__c DESC limit 1];
        if(last.To_Date__c == null )
        { 
        if(!(mydate<=last.From_Date__c)){
          last.To_Date__c=mydate.addDays(-1);
          update(last);
          }
        }
        }catch(Exception e)
        {
        
        }
        
        
        try {
                  upsert(t);
                } catch(System.DMLException e) {
               ApexPages.addMessages(e);
               pref.setRedirect(true);
               return pref;
                }
         pref.setRedirect(true);        
        return pref;
    }

     public List<TA__c> tadall;
     
    public List<TA__c> getTadAll(){
    tadall=[Select id,Name,To_Date__c,From_Date__c,TA_Value__c From TA__c ORDER BY From_Date__c DESC];
    return tadall;
    }
}