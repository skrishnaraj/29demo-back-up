public with sharing class EventCalculator{

    public PageReference doSomething() {
    dateParam = ApexPages.currentPage().getParameters().get('dateSelectedOnCalendar');
    initializeSOQL();
        return null;
    }

 // Event Calculator  
    public PageReference leadhome(){
    String a=ApexPages.currentPage().getParameters().get('id');
    PageReference l=new PageReference('/'+a);
    return l;
    }
 

public String SelectedUser { get; set{SelectedUser=value;} }
 //public Boolean firstB = false;
public Boolean renderFieldRepColumn = true;
list<Lead__c> lead=new List<Lead__c>();

String dateParam;
String dateValue;
public Date givenDate;
public String RecordTypeName;
public EventCalculator(){    
    dateParam = ApexPages.currentPage().getParameters().get('appDate');
    if (dateParam == null || dateParam == ''){
        dateParam = Date.today().format();
    }
 
 string profileId = UserInfo.getProfileId();
 Profile p = [select id,Name from profile where id=:profileId];
 for(CollectionProfile__c c:[Select id,Name from CollectionProfile__c])
 {
 if(c.Name == p.Name)
 {
 	RecordTypeName = 'Collection';
 }
 }
 for(SalesProfile__c c:[Select id,Name from SalesProfile__c])
 {
 if(c.Name == p.Name)
 {
 	RecordTypeName = 'Sales';
 }
 }
    
    initializeSOQL(); 
}

  void initializeSOQL(){
if(dateParam != null){

        con = null;
        givenDate = Date.parse(dateParam );
        String day,month;
        if(String.valueOf(givenDate.month()).length() == 2){
            month = String.valueOf(givenDate.month());
        } 
        else{
            month = '0' + String.valueOf(givenDate.month());
        }
        if(String.valueOf(givenDate.day()).length() == 2){
            day = String.valueOf(givenDate.day());
        } 
        else{
            day = '0' + String.valueOf(givenDate.day());
        } 
        dateValue = givenDate.year() + '-' + month+ '-' + day;

        if(selectedUser != null && selectedUser != 'All' ){
        
           soql ='SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c = \'' + SelectedUser + '\' and Appointment_Date__c ='+dateValue+'  order by Appointment_Date_Time__c ';
        }
        else{
        soql ='SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c != null and Appointment_Date__c ='+dateValue+'  order by Appointment_Date_Time__c ';
        }
        originalSOQL= 'SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c != null and Appointment_Date__c ='+dateValue ;
    }
}


private String soql ='SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c != null order by Appointment_Date_Time__c ';
string originalSOQL= 'SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c != null ' ;
    
     public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;}
    set;
  }
 public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }

   
    public PageReference toggleSort() {

    lead.clear();
    con = null;
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    soql = originalSOQL.split('order')[0] + ' order by ' + sortField + ' ' + sortDir ;
    // system.assertEquals(soql,null);
     return null;
  }
    
    
    
    public PageReference find() {
     con = null;
    if(SelectedUser == 'All')
    {
    //firstB = true;
    originalSOQL = 'SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c != null order by Appointment_Date_Time__c ';
        if(dateValue != null)
        {
          soql='SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c != null and Appointment_Date__c ='+dateValue  ;
        }
        if(dateValue == null)
        {    
        soql=originalSOQL;
        }
     renderFieldRepColumn = true;
    }
    else if(SelectedUser != 'All'){
    renderFieldRepColumn = false;
    if(dateValue != null)
        {
          soql ='SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c= \''+SelectedUser + '\' and Appointment_Date__c = ' + dateValue ;
        }
        if(dateValue == null)
        {    
        soql='SELECT Id,Name,Lead_Name__c,Appointment_Date_Time__c,Street__c,City__c,State_Province__c,Zipcode__c,Field_Rep__c FROM Lead__c where Field_Rep__c= \''+SelectedUser +'\' order by Appointment_Date__c';
        }
        //originalSOQL = soql;
       // firstB = true;
    
    }
        return null;
    }


    public Boolean getRenderFieldRepColumn(){
    
    return renderFieldRepColumn; 
    }
  
public List<Lead__c> getEventDetails()
 {
 lead.clear();
  for(Lead__c leadRec : (List<Lead__c>)con.getRecords())
        {
            lead.add(leadRec);
        }
        return lead;
  
 }

 public List<SelectOption> getUsDetails()
 {
 List<SelectOption> options=new List<SelectOption>();
 
 options.add(new selectOption('All','ALL'));
 String sysAdminProfileId = [select Id from Profile where Name like '%System Administrator'].Id;
    Id manager=UserInfo.getUserId();
    String currentUserProfileId = UserInfo.getProfileId();
    if(sysAdminProfileId == currentUserProfileId ){
        for(FieldRep__c frep:[select id,name from FieldRep__c where Active__c = true])
         {
         options.add(new selectOption(frep.id,frep.Name));
         }
    }
    else{
         for(FieldRep__c frep:[select id,name from FieldRep__c where Active__c = true and reporting_manager__c=:manager])
         {
         options.add(new selectOption(frep.id,frep.Name));
         }
    }
 return options;
 
 }
 // Pagination starts from here
 public ApexPages.StandardSetController con {
        get {
            if(con == null) {
                con = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
                // sets the number of records in each page set
                con.setPageSize(5);
            }
            return con;
        }
        set;
    }
 // indicates whether there are more records after the current page set.
 public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
 // returns the page number of the current page set
 public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
 
    // returns the first page of records
    public void first() {
        con.first();
    }
 
    // returns the last page of records
    public void last() {
        con.last();
    }
 
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
 
    // returns the next page of records
    public void next() {
        con.next();
    }


}