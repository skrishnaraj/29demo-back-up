@RestResource(urlMapping='/PushDataToServer1/*')

global class PushDataWebService {
    

global class ParametersClass extends LeadRecords{
     String ZipCode{get;set;}
     String State{get;set;}
     String City{get;set;}
     String Street_Address{get;set;}
     String Phone_Number{get;set;}
     String Email{get;set;}
     String ActivityName { get;set;}
     
}


@HttpPost   
  global static String upsertLeadRecord() {
    Map<String,String> documentsMap = new Map<String,String>();
    RestRequest request = RestContext.request;
    RestResponse res = RestContext.response;
    ParametersClass recordDetails;
    // the record to be updated is sent across as a JSON object(blob). we need to convert it into a JSONstring and then deserialize it into ParametersClass object 
    try{
         recordDetails = (ParametersClass)JSON.deserialize(request.requestBody.toString(),ParametersClass.class);
         system.debug(recordDetails + 'RECORD DETAILS');
    }
    catch(Exception ex){
        res.addHeader('Custom_Status_Code','402');
        return null;
    }
    FieldRep__c rep;
    //system.assertEquals(recordDetails,null);
    try{
     rep = [select Id,Full_Name__c from FieldRep__c where IMEI_Number__c = :recordDetails.IMEI_Number and Active__c = true];
    }
    catch(Exception e){
        if(e.getMessage().contains('List has no rows for assignment')){
            res.addHeader('Custom_Status_Code', '400');
            return null;
        }
     }
     Location_Data__c ld = new Location_Data__c();
     ld.FieldRep__c = rep.Id;
     ld.Latitude__c = recordDetails.Latitude;
     ld.Longitude__c = recordDetails.Longitude;
     String tempDateTime = Date.today().format() + '' + recordDetails.CapturedDateTime.substring(recordDetails.CapturedDateTime.length()-9, recordDetails.CapturedDateTime.length());
     ld.CapturedDateTime__c = Datetime.parse(tempDateTime);
     if(recordDetails.Id != null & recordDetails.Id != '')
     ld.Related_Record__c = recordDetails.Id;
     try{
        database.insert(ld);
     }
     catch(Exception exc){
        res.addHeader('Custom_Status_Code', '403');
     }
  if(recordDetails.Id !=null && recordDetails.Id != ''){
    Lead__c record =[select Id,Lead_Name__c,SR_Appointment_Status__c,Field_Rep__c,SR_Appointment_Comment__c,Sharing_reason__c from Lead__c where Id=:recordDetails.Id];
    if(recordDetails.ActivityName =='Sales'){ 
         record.SR_Appointment_Comment__c= recordDetails.SR_Appointment_Comment;
         record.SR_Appointment_Status__c = recordDetails.SR_Appointment_Status;
         if(record.Field_Rep__c != rep.Id){
            record.Field_Rep__c = rep.Id;
         }
         if(recordDetails.documentsList!= null && recordDetails.documentsList.size()>0){
            for(LeadRecords.LeadDocument doc :recordDetails.documentsList){
                if(doc.FilePath != null){
                    documentsMap.put(doc.Name,doc.FilePath);
                    //system.debug(doc.FilePath,null);
                }
            }
            if(documentsMap.size() >0){
                //system.debug(documentsMap + 'DOC DETAILS');
                Set<String> docNames = documentsMap.keySet();
                List<String> docNamesList = new List<String>();
                List<DocumentLink__c> docsToUpdate = new List<DocumentLink__c>();
                docNamesList.addAll(docNames);
                for(ScannedDocument__c doc : [select Name,Id,Document_Name__c from ScannedDocument__c where Lead__c = :recordDetails.Id and Document_Name__c IN :docNamesList ]){
                    String temp = doc.Document_Name__c;
                    if(documentsMap.containsKey(temp)){
                        List<String> links = documentsMap.get(temp).split(',');
                        for(Integer i = 0; i<links.size();i++){
                            if(links[i] == 'null'){
                                continue;
                            }
                            else{
                                DocumentLink__c linkToCreate= new DocumentLink__c();
                                linkToCreate.DocumentLink__c = (links[i].length()>255)?links[i].substring(0,254):links[i];
                                linkToCreate.ParentDocument__c = doc.Id;
                                docsToUpdate.add(linkToCreate);
                            }
                        }  
                    }
                }
                if(docsToUpdate.size() > 0){
                    try{
                    database.update(docsToUpdate);
                    }
                    catch(Exception e1){
                        res.addHeader('Custom_Status_Code', '404');
                    }
                }
            }
         }
         try{
            database.update(record);
         }
         catch(Exception e2){
                res.addHeader('Custom_Status_Code', '405');
         }
         res.addHeader('Custom_Status_Code', '201');
    }
    else if(recordDetails.ActivityName == 'Collection'){
        Task fieldTask = new Task();
        fieldTask.Subject = 'Contacted ' + record.Lead_Name__c + ' on ' + date.today().format() + '(' + record.Sharing_reason__c + ')';
        fieldTask.Description = recordDetails.SR_Appointment_Comment;
        fieldTask.WhatId = record.Id;
        fieldTask.Status = 'Completed';
        fieldTask.Priority = 'Normal';
        fieldTask.Assigned_To__c = rep.Full_Name__c;
        fieldTask.Activity_Type__c = 'Field';
        database.insert(fieldTask);
        record.SR_Appointment_Comment__c= recordDetails.SR_Appointment_Comment;
        database.update(record);
        if(recordDetails.documentsList!= null && recordDetails.documentsList.size()>0){
            for(LeadRecords.LeadDocument doc :recordDetails.documentsList){
                if(doc.FilePath != null){
                    documentsMap.put(doc.Name,doc.FilePath);
                    //system.debug(doc.FilePath,null);
                }
            }
            if(documentsMap.size() >0){
                //system.debug(documentsMap + 'DOC DETAILS');
                Set<String> docNames = documentsMap.keySet();
                List<String> docNamesList = new List<String>();
                List<DocumentLink__c> docsToUpdate = new List<DocumentLink__c>();
                docNamesList.addAll(docNames);
                for(ScannedDocument__c doc : [select Name,Id,Document_Name__c from ScannedDocument__c where Lead__c = :recordDetails.Id and Document_Name__c IN :docNamesList ]){
                    String temp = doc.Document_Name__c;
                    if(documentsMap.containsKey(temp)){
                        List<String> links = documentsMap.get(temp).split(',');
                        for(Integer i = 0; i<links.size();i++){
                            if(links[i] == 'null'){
                                continue;
                            }
                            else{
                                DocumentLink__c linkToCreate= new DocumentLink__c();
                                linkToCreate.DocumentLink__c = (links[i].length()>255)?links[i].substring(0,254):links[i];
                                linkToCreate.ParentDocument__c = doc.Id;
                                docsToUpdate.add(linkToCreate);
                            }
                        }  
                    }
                }
                if(docsToUpdate.size() > 0){
                    try{
                    database.update(docsToUpdate);
                    }
                    catch(Exception e1){
                        res.addHeader('Custom_Status_Code', '404');
                    }
                }
            }
         }
    }
     return null;
     }
     else{
         List<ScannedDocument__c> docsToInsert = new List<ScannedDocument__c>();
         Lead__c newLead = new Lead__c();
         newLead.Lead_Name__c = recordDetails.Name;
         newLead.SR_Appointment_Comment__c=recordDetails.SR_Appointment_Comment;
         newLead.SR_Appointment_Status__c = recordDetails.SR_Appointment_Status;
        if(recordDetails.Email != null && recordDetails.Email != '')
         newLead.Email_Id__c = recordDetails.Email;
         if(recordDetails.Phone_Number != null && recordDetails.Phone_Number != '')
         newLead.Phone__c = recordDetails.Phone_Number;
         if(recordDetails.ZipCode != null && recordDetails.ZipCode != '')
         newLead.ZipCode__c = recordDetails.ZipCode;
         if(recordDetails.Street_Address != null && recordDetails.Street_Address != '')
         newLead.Street__c = recordDetails.Street_Address;
         if(recordDetails.State != null && recordDetails.State != '')
         newLead.State_Province__c = recordDetails.State;
         if(recordDetails.City != null && recordDetails.City != '')
         newLead.City__c = recordDetails.City; 
         try{
         database.insert(newLead);
         }
         catch(Exception e3){
                res.addHeader('Custom_Status_Code', '406');
        }
        if(recordDetails.documentsList.size()>0){
        for(LeadRecords.LeadDocument doc :recordDetails.documentsList){
            
            if(doc.FilePath != null){
                documentsMap.put(doc.Name,doc.FilePath);
                ScannedDocument__c uploadedDoc = new ScannedDocument__c();
                uploadedDoc.Document_Name__c = doc.Name;
                uploadedDoc.Lead__c = newLead.Id;
                uploadedDoc.Number_Of_Pages__c = doc.numberOfImages;
                docsToInsert.add(uploadedDoc);
                //system.assertEquals(doc.FilePath,null);
            }
        }
        if(documentsMap.size() >0){
            system.debug(documentsMap + '  Test Map');
            List<String> docIds = new List<String>();
            Database.SaveResult[] results = database.insert(docsToInsert);
            for(Database.SaveResult result:results){
                if(result.isSuccess()){
                    docIds.add(result.getId());
                }
            }
            List<DocumentLink__c> linksToInsert = new List<DocumentLink__c>();
            for(ScannedDocument__c doc : [select Name,Id,Document_Name__c from ScannedDocument__c where ID IN :docIds]){
                String temp = doc.Document_Name__c;
                if(documentsMap.containsKey(temp)){
                    List<String> links = documentsMap.get(temp).split(',');
                    for(Integer i = 0; i<links.size();i++){
                        if(links[i] == 'null'){
                            continue;
                        }
                        else{
                            DocumentLink__c linkToCreate= new DocumentLink__c();
                            linkToCreate.DocumentLink__c = (links[i].length()>255)?links[i].substring(0,254):links[i];
                            linkToCreate.ParentDocument__c = doc.Id;
                            linksToInsert.add(linkToCreate);
                        }
                    }  
                }
                }
            if(linksToInsert.size() > 0){
                try{
                database.insert(linksToInsert);
                //system.assertEquals(docsToInsert,null);
                }
                catch(Exception e1){
                    res.addHeader('Custom_Status_Code', '404');
                }
            }
        }
    }
         res.addHeader('Custom_Status_Code', '202');
         return null;
     }

return null;
     }
}