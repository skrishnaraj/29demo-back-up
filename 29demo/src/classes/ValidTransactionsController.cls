public class ValidTransactionsController {
    private final Lead__c leadRec ;
    
    public Boolean renderSection2;
    public Boolean getRenderSection2(){
    return renderSection2;
    }
    public Boolean renderSection1  = true;
    public Boolean getRenderSection1(){
    return renderSection1;
    }
    Integer counter = 0;
    List<Transaction__c> transactionsList1 = new List<Transaction__c>();
    List<Transaction__c> transactionsList2 = new List<Transaction__c>();
    
    public ValidTransactionsController(ApexPages.StandardController controller) {
        leadRec = (Lead__c)controller.getRecord();
        String render1 = ApexPages.currentPage().getParameters().get('renderSection1');
        String render2 = ApexPages.currentPage().getParameters().get('renderSection2');
        if(render1 != null)
          {
          renderSection1 = Boolean.ValueOf(render1);
          }
          if(render2 != null)
          {
          renderSection2 = Boolean.ValueOf(render2);
          }
        
        for(Transaction__c tr :[select id,Name,Service_Name__r.Name,Service_Name__r.Settlement_Amount__c,Service_Name__r.card_loan_no__c,Amount_To_be_Paid__c,Validity_Date__c,Bill_Date__c,Completed__c from Transaction__c where Lead__c =:leadRec.Id and Ignored_transaction__c=false]){
            if(counter<5){
                transactionsList1.add(tr);
                counter++;
            }
            else if(counter == 5){
               transactionsList2.addAll(transactionsList1); 
               transactionsList2.add(tr);
               counter++;
                }
            else{
               transactionsList2.add(tr);
               counter++;
                }
          }
       }
    
    public List<Transaction__c> getTrans(){
        return transactionsList1;
    }
    
    public List<Transaction__c> getTransactions(){
         return transactionsList1;
    }
    
    public List<Transaction__c> getAllTransactions(){
         return transactionsList2;
    }
    
    public Integer getListSize(){
        return counter;
    }
    
    public PageReference doNothing(){
        return null;
    }
    
    public PageReference saveTransaction(){
    update(transactionsList1);
        return null;
    }
    public PageReference cancelTransaction(){
        return null;
    }
    
    public PageReference saveTransaction2(){
    update(transactionsList2);
        return null;
    }
   

}