trigger Lead_AfterUpdate_Trigger on Lead__c (after update) {
    List<Contact_numbers__c> recsToInsert = new List<Contact_numbers__c>();
    
    // collect all transaction data - only to mark transactions as IGNORED if appoval status like "Settlement rejected" 
    List<Transaction__c> transactionsToUpdate = new List<Transaction__c>(); 
    Map<String,List<Transaction__c>> leadIdTransactionsMap = new Map<String,List<Transaction__c>>();  
    String currentLead = null;
    List<Transaction__c> tempList = new List<Transaction__c>(); 
    for(Transaction__c tr : [select Id,Lead__c,Ignored_transaction__c from Transaction__c where Lead__c IN :trigger.newMap.keySet() order by Lead__c limit 10000]){
        if(currentLead == null){
            currentLead = tr.Lead__c;
            tempList.add(tr);
        }
        else if(currentLead == tr.Lead__c){
            tempList.add(tr);
        }
        else{
            //map of lead and its related transactions
            leadIdTransactionsMap.put(currentLead,tempList);
            currentLead = tr.Lead__c;
            tempList.clear();
            templist.add(tr);
        }
    }
    //add last lead and its transactions to the map
    if(templist.size() > 0 && currentLead != null){
        leadIdTransactionsMap.put(currentLead,tempList);
    }
    
    for(Lead__c leadRec : Trigger.new){
    //update contact numbers
        if(Trigger.oldMap.get(leadRec.Id).Phone__c !=null && Trigger.oldMap.get(leadRec.Id).Phone__c != leadRec.Phone__c){
            Contact_numbers__c newRec = new Contact_numbers__c();
            newRec.Contact_Number__c = Trigger.oldMap.get(leadRec.Id).Phone__c;
            newRec.Lead__c = leadRec.Id;
            newRec.Updated_Number__c = Schema.Lead__c.fields.Phone__c.getDescribe().getLabel();
            recsToInsert.add(newRec);
        }
        if(Trigger.oldMap.get(leadRec.Id).Current_Phone_Number__c != null && Trigger.oldMap.get(leadRec.Id).Current_Phone_Number__c != leadRec.Current_Phone_Number__c){
            Contact_numbers__c newRec = new Contact_numbers__c();
            newRec.Contact_Number__c = Trigger.oldMap.get(leadRec.Id).Current_Phone_Number__c ;
            newRec.Lead__c = leadRec.Id;
            newRec.Updated_Number__c = Schema.Lead__c.fields.Current_Phone_Number__c.getDescribe().getLabel();
            recsToInsert.add(newRec);
        }
        if(Trigger.oldMap.get(leadRec.Id).Current_Phone_Numbers__c!= leadRec.Current_Phone_Numbers__c && leadRec.Current_Phone_Numbers__c.contains(';')){
            List<String> numbers = leadRec.Current_Phone_Numbers__c.split(';');
            for(Integer i = 1; i< numbers.size();i++){
                Contact_numbers__c newRec = new Contact_numbers__c();
                newRec.Contact_Number__c = numbers[i];
                newRec.Lead__c = leadRec.Id;
                newRec.Updated_Number__c = Schema.Lead__c.fields.Current_Phone_Number__c.getDescribe().getLabel();
                recsToInsert.add(newRec);
            }
        }
        if(Trigger.oldMap.get(leadRec.Id).HC_Contact_Number__c!=null && Trigger.oldMap.get(leadRec.Id).HC_Contact_Number__c!= leadRec.HC_Contact_Number__c){
            Contact_numbers__c newRec = new Contact_numbers__c();
            newRec.Contact_Number__c = Trigger.oldMap.get(leadRec.Id).HC_Contact_Number__c;
            newRec.Lead__c = leadRec.Id;
            newRec.Updated_Number__c = Schema.Lead__c.fields.HC_Contact_Number__c.getDescribe().getLabel();
            recsToInsert.add(newRec);
        }
        if(Trigger.oldMap.get(leadRec.Id).HC_Contact_Numbers__c!= leadRec.HC_Contact_Numbers__c && leadRec.HC_Contact_Numbers__c.contains(';')){
            List<String> numbers = leadRec.HC_Contact_Numbers__c.split(';');
            for(Integer i = 1; i< numbers.size();i++){
                Contact_numbers__c newRec = new Contact_numbers__c();
                newRec.Contact_Number__c = numbers[i];
                newRec.Lead__c = leadRec.Id;
                newRec.Updated_Number__c = Schema.Lead__c.fields.HC_Contact_Number__c.getDescribe().getLabel();
                recsToInsert.add(newRec);
            }
        }
        if(Trigger.oldMap.get(leadRec.Id).Reference_contact_Number__c !=null && Trigger.oldMap.get(leadRec.Id).Reference_contact_Number__c != leadRec.Reference_contact_Number__c){
            Contact_numbers__c newRec = new Contact_numbers__c();
            newRec.Contact_Number__c = Trigger.oldMap.get(leadRec.Id).Reference_contact_Number__c;
            newRec.Lead__c = leadRec.Id;
            newRec.Updated_Number__c = Schema.Lead__c.fields.Reference_contact_Number__c.getDescribe().getLabel();
            recsToInsert.add(newRec);
        }
        if(Trigger.oldMap.get(leadRec.Id).Reference_contact_Numbers__c!= leadRec.Reference_contact_Numbers__c && leadRec.Reference_contact_Numbers__c.contains(';')){
            List<String> numbers = leadRec.Reference_contact_Numbers__c.split(';');
            for(Integer i = 1; i< numbers.size();i++){
                Contact_numbers__c newRec = new Contact_numbers__c();
                newRec.Contact_Number__c = numbers[i];
                newRec.Lead__c = leadRec.Id;
                newRec.Updated_Number__c = Schema.Lead__c.fields.Reference_contact_Number__c.getDescribe().getLabel();
                recsToInsert.add(newRec);
            }
        }
        if(Trigger.oldMap.get(leadRec.Id).UAE_Contact_Number__c != null && Trigger.oldMap.get(leadRec.Id).UAE_Contact_Number__c != leadRec.UAE_Contact_Number__c){
            Contact_numbers__c newRec = new Contact_numbers__c();
            newRec.Contact_Number__c = Trigger.oldMap.get(leadRec.Id).UAE_Contact_Number__c;
            newRec.Lead__c = leadRec.Id;
            newRec.Updated_Number__c = Schema.Lead__c.fields.UAE_Contact_Number__c.getDescribe().getLabel();
            recsToInsert.add(newRec);
        }
        if(Trigger.oldMap.get(leadRec.Id).UAE_Contact_Numbers__c!= leadRec.UAE_Contact_Numbers__c && leadRec.UAE_Contact_Numbers__c.contains(';')){
            List<String> numbers = leadRec.UAE_Contact_Numbers__c.split(';');
            for(Integer i = 1; i< numbers.size();i++){
                Contact_numbers__c newRec = new Contact_numbers__c();
                newRec.Contact_Number__c = numbers[i];
                newRec.Lead__c = leadRec.Id;
                newRec.Updated_Number__c = Schema.Lead__c.fields.UAE_Contact_Number__c.getDescribe().getLabel();
                recsToInsert.add(newRec);
            }
        }
        
        //call approval process if criteria is met
        if (Trigger.oldMap.get(leadRec.Id).Approval_Status__c != 'Field Approval Pending' && leadRec.Approval_Status__c == 'Field Approval Pending') {
 
            // create the new approval request to submit
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(leadRec.Id);
            req.setNextApproverIds(new Id[] {leadRec.OwnerId});
            // submit the approval request for processing
            Approval.ProcessResult result = Approval.process(req);
            // display if the reqeust was successful
            System.debug('Submitted for approval successfully: '+result.isSuccess());
 
        }
        
        if (Trigger.oldMap.get(leadRec.Id).Approval_Status__c != 'Settlement Approval Pending' && leadRec.Approval_Status__c == 'Settlement Approval Pending') {
 
            // create the new approval request to submit
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(leadRec.Id);
            req.setNextApproverIds(new Id[] {leadRec.Telecaller_Manager_Id__c});
            // submit the approval request for processing
            try{
                Approval.ProcessResult result = Approval.process(req);
            }
            catch(Exception e){
                //leadRec.addError('There is already an approval process in progress');
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
                ApexPages.addMessage(myMsg);
            }
            // display if the reqeust was successful

 
        }
        if (Trigger.oldMap.get(leadRec.Id).Approval_Status__c != 'Settlement Rejected' && leadRec.Approval_Status__c == 'Settlement Rejected') {
            List<Transaction__c> temp = leadIdTransactionsMap.get(leadRec.Id);
            if(temp != null){
             for(Transaction__c trToUpdate : temp){
                trToUpdate.Ignored_transaction__c = true;
                transactionsToUpdate.add(trToUpdate);
             }
            }
        }
        
    }
    if(recsToInsert.size()>0){
        database.insert(recsToInsert);
    }
    if(transactionsToUpdate.size() > 0){
        database.update(transactionsToUpdate);
    }
}