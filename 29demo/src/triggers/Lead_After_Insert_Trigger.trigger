trigger Lead_After_Insert_Trigger on Lead__c (after insert) {
List<Contact_numbers__c> recsToMassInsert = new List<Contact_numbers__c>();
    //mass update contact numbers using trigger
    if (Trigger.isInsert) {
        for(Lead__c lead : Trigger.new){
            if(lead.HC_Contact_Numbers__c != null){
                List<String> contactNumbers = lead.HC_Contact_Numbers__c.split(';');
                for(Integer i=1;i <contactNumbers.size();i++){
                    Contact_numbers__c newRec = new Contact_numbers__c();
                    newRec.Contact_Number__c = contactNumbers[i];
                    newRec.Lead__c = lead.Id;
                    newRec.Updated_Number__c = Schema.Lead__c.fields.HC_Contact_Number__c.getDescribe().getLabel();
                    recsToMassInsert.add(newRec);
                }
            }
            if(lead.UAE_Contact_Numbers__c != null){
                List<String> contactNumbers = lead.UAE_Contact_Numbers__c.split(';');
                for(Integer i=1;i <contactNumbers.size();i++){
                    Contact_numbers__c newRec = new Contact_numbers__c();
                    newRec.Contact_Number__c = contactNumbers[i];
                    newRec.Lead__c = lead.Id;
                    newRec.Updated_Number__c = Schema.Lead__c.fields.UAE_Contact_Number__c.getDescribe().getLabel();
                    recsToMassInsert.add(newRec);
                }
            }
            if(lead.Reference_Contact_Numbers__c != null){
                List<String> contactNumbers = lead.Reference_Contact_Numbers__c.split(';');
                for(Integer i=1;i <contactNumbers.size();i++){
                    Contact_numbers__c newRec = new Contact_numbers__c();
                    newRec.Contact_Number__c = contactNumbers[i];
                    newRec.Lead__c = lead.Id;
                    newRec.Updated_Number__c = Schema.Lead__c.fields.Reference_Contact_Number__c.getDescribe().getLabel();
                    recsToMassInsert.add(newRec);
                }
            }
            
        }
        if(recsToMassInsert.size()>0){
                    //system.assertEquals(recsToMassInsert,null);
                    database.insert(recsToMassInsert);
        }
    }
}