trigger updateTellecaller on Call_log__c (before insert,before update) {

 Map<String,String> assignLead = new Map<String,String>();
 Set<String> agssignTo = new Set<String>();
 
 for(Call_log__c callLog : Trigger.new){
    agssignTo.add(callLog.Assigned_To__c);
    
  }
 
 List<Task> activityHistroyList =[select Phone__c,whatId FROM task where status = 'Completed' and Activity_type__c = 'Default' and phone__c != null and createdDate = LAST_N_DAYS:30];
 for(Task t:activityHistroyList){
  if(t.Phone__c.length() > 10){
   String phoneNo = t.Phone__c.substring(t.Phone__c.length()-10,t.Phone__c.length());
   assignLead.put(phoneNo,t.WhatId);
 }
 else{
   assignLead.put(t.Phone__c,t.WhatId);
 
 }
 }
 
 List<Telecaller__c> tel = [Select Id,Name from Telecaller__c Where Name in :agssignTo];
 
 Map<String,String> assignTelecaller  = new Map<String,String>();
 
 for(Telecaller__c t : tel)
 {
    assignTelecaller.put(t.Name,t.Id);
 }
 
 //Map<Id,Telecaller__c> assignTelecaller = new Map<Id,Telecaller__c>([Select Id from Telecaller__c Where Name in :agssignTo]);
 
 for(Call_log__c callLog : Trigger.new){
    String phoneNumber ='';
    if(callLog.IN_Out__c.length() > 10){
        phoneNumber = callLog.IN_Out__c.substring(callLog.IN_Out__c.length()-10,callLog.IN_Out__c.length());
    }
    else{
        phoneNumber = callLog.IN_Out__c;
    }
  callLog.Lead__c = assignLead.get(phoneNumber);
  callLog.Telecaller__c = assignTelecaller.get(callLog.Assigned_To__c);
  String callStartEndTime = callLog.Call_Start_End_text__c;
  String timeSpan = callLog.Time_span_Text__c;
  callLog.Start_Time__c = callStartEndTime.split('-')[0];
  callLog.End_Time__c = callStartEndTime.split('-')[1];
  callLog.Time_Span1__c = timeSpan.split('-')[0];
  callLog.Time_Span2__c = timeSpan.split('-')[1];
 }
}