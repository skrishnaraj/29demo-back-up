trigger Lead_BI_BU_Trigger on Lead__c (before insert , before update) {

    //mass update contact numbers using trigger
    Map<String,String> CIS_AssignedTo_Map = new Map<String,String>();
    for(Lead__c lead: Trigger.new){
        if(lead.Assigned_To__c !=null){
            CIS_AssignedTo_Map.put(lead.CIS_No__c,lead.Assigned_To__c.toUpperCase());
            }
    }
    Map<String,String> leadId_QueueId_Map = new Map<String,String>();
    Map<String,String> leadId_TeleCallerId_Map = new Map<String,String>();
    for(QueueSobject queue : [select QueueId,Queue.Name from QueueSobject q  ]){
        leadId_QueueId_Map.put((queue.Queue.Name).toUpperCase(),queue.QueueId);
    }
    for(Telecaller__c rec : [select Id,Name from Telecaller__c ]){
        leadId_TeleCallerId_Map.put((rec.Name).toUpperCase(),rec.Id);
	}
    
        if (Trigger.isInsert) {
            
    
            for(Lead__c lead : Trigger.new){
                if(lead.UAE_Contact_Numbers__c != null){
                    List<String> contactNumbers = lead.UAE_Contact_Numbers__c.split(';');
                    lead.UAE_Contact_Number__c = contactNumbers[0];
                }
                if(lead.HC_Contact_Numbers__c != null){
                    List<String> contactNumbers = lead.HC_Contact_Numbers__c.split(';');
                    lead.HC_Contact_Number__c = contactNumbers[0];
                }
                if(lead.Reference_Contact_Numbers__c != null){
                    List<String> contactNumbers = lead.Reference_Contact_Numbers__c.split(';');
                    lead.Reference_Contact_Number__c = contactNumbers[0];
                }
                if(lead.Assigned_To__c != null){
                    String valueFromMap = leadId_QueueId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                    if(valueFromMap != null){
                        lead.OwnerId = leadId_QueueId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                    }
                }   
                //insert telecaller field
                String valueFromMap = leadId_TeleCallerId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                if(valueFromMap != null){
                    lead.Telecaller__c = leadId_TeleCallerId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                }
            }
        }
    
    
    if (Trigger.isUpdate) {
        Map<String,String> OwnerIdLeadIdMap = new Map<String,String>();
        for(Lead__c lead: Trigger.new){
                OwnerIdLeadIdMap.put(lead.OwnerId,lead.Id);
        }
        Map<String,String> QueueIdQueueOwner_Map = new Map<String,String>();
        Map<String,String> TelecallerName_Id_Map = new Map<String,String>();
        for(QueueSobject queue : [select QueueId,Queue.Name from QueueSobject q where q.QueueId IN :OwnerIdLeadIdMap.keySet()]){
            QueueIdQueueOwner_Map.put(queue.QueueId,queue.Queue.Name.toUpperCase());
        }
        for(Telecaller__c rec : [select Id,Name from Telecaller__c where Name IN :QueueIdQueueOwner_Map.values()]){
            TelecallerName_Id_Map.put(rec.Name.toUpperCase(),rec.Id);
        }
        for(Lead__c lead : Trigger.new){     
        	if(Trigger.oldMap.get(lead.Id).OwnerId != lead.OwnerId){   
	            if(lead.Assigned_To__c!= null && lead.Assigned_To__c != QueueIdQueueOwner_Map.get(lead.OwnerId)){
	                lead.Assigned_to__c = QueueIdQueueOwner_Map.get(lead.OwnerId);
	            }
	            //update telecaller field
	            String valueFromMap = TelecallerName_Id_Map.get(QueueIdQueueOwner_Map.get(lead.OwnerId));
	            if(valueFromMap != null){
	                lead.Telecaller__c = valueFromMap;
	            }
        	}
        	else if(Trigger.oldMap.get(lead.Id).Assigned_To__c != lead.Assigned_To__c){
        		if(lead.Assigned_To__c != null){
                    String valueFromMap = leadId_QueueId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                    if(valueFromMap != null){
                        lead.OwnerId = leadId_QueueId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                    }
                }   
                //insert telecaller field
                String valueFromMap = leadId_TeleCallerId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                if(valueFromMap != null){
                    lead.Telecaller__c = leadId_TeleCallerId_Map.get(CIS_AssignedTo_Map.get(lead.CIS_No__c));
                }
        	}
            //revoke sharing
            if(lead.Approval_Status__c == 'Field Visit Approved'){
                lead.Share_Record_With__c = null;
            }
            if(lead.HC_Contact_Number__c!=null && Trigger.oldMap.get(lead.Id).HC_Contact_Number__c != lead.HC_Contact_Number__c && lead.HC_Contact_Number__c.contains(';')){
                lead.HC_Contact_Numbers__c = lead.HC_Contact_Number__c;
                lead.HC_Contact_Number__c = lead.HC_Contact_Number__c.split(';')[0];
            }
            if(lead.UAE_Contact_Number__c!=null && Trigger.oldMap.get(lead.Id).UAE_Contact_Number__c != lead.UAE_Contact_Number__c && lead.UAE_Contact_Number__c.contains(';')){
                lead.UAE_Contact_Numbers__c = lead.UAE_Contact_Number__c;
                lead.UAE_Contact_Number__c = lead.UAE_Contact_Number__c.split(';')[0];
            }
            if(lead.Reference_Contact_Number__c!=null && Trigger.oldMap.get(lead.Id).Reference_Contact_Number__c != lead.Reference_Contact_Number__c && lead.Reference_Contact_Number__c.contains(';')){
                lead.Reference_Contact_Numbers__c = lead.Reference_Contact_Number__c;
                lead.Reference_Contact_Number__c = lead.Reference_Contact_Number__c.split(';')[0];
            }
            
            if(lead.Current_Phone_Number__c!= null && Trigger.oldMap.get(lead.Id).Current_Phone_Number__c != lead.Current_Phone_Number__c && lead.Current_Phone_Number__c.contains(';')){
                lead.Current_Phone_Numbers__c = lead.Current_Phone_Number__c;
                lead.Current_Phone_Number__c = lead.Current_Phone_Number__c.split(';')[0];
            }
            

        }
    }

}