trigger Call_Log_Before_Insert_Update on Call_log__c (before insert,before update) {

 Map<String,String> assignTelecaller  = new Map<String,String>();
 Map<String,String> assignLead = new Map<String,String>();
 Set<String> agssignTo = new Set<String>();
 Set<String> callIds = new Set<String>();
 
 
 for(Call_log__c callLog : Trigger.new){
     agssignTo.add(callLog.Assigned_To__c);
     callIds.add(callLog.Call_ID__c);
 }
 
 for(Task t:[select Phone__c,whatId,Call_id__c FROM task where status = 'Completed' and Activity_type__c = 'Default' and Call_id__c IN :callIds ]){
       assignLead.put(t.Call_id__c,t.WhatId);
 }
 
 for(Telecaller__c t : [Select Id,Name from Telecaller__c Where Name in :agssignTo])
 {
    assignTelecaller.put(t.Name,t.Id);
 }
 
 for(Call_log__c callLog : Trigger.new){
      callLog.Lead__c = assignLead.get(callLog.Call_Id__c);
      callLog.Telecaller__c = assignTelecaller.get(callLog.Assigned_To__c);
      String callStartEndTime = callLog.Call_Start_End_text__c;
      String timeSpan = callLog.Time_span_Text__c;
      callLog.Start_Time__c = callStartEndTime.split('-')[0];
      callLog.End_Time__c = callStartEndTime.split('-')[1];
      callLog.Time_Span1__c = timeSpan.split('-')[0];
      callLog.Time_Span2__c = timeSpan.split('-')[1];
 }
 

 
}