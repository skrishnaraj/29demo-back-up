trigger Call_Log_After_Insert_Update on Call_log__c (after insert, after update) {
    
    Map<String,Call_Log__c> callIds = new Map<String,Call_Log__c>();
    List<Task> tasksToUpdate = new List<Task>();
    
    for(Call_log__c cl :Trigger.new){
        callIds.put(cl.Call_Id__c,cl);
    }
    
    for(Task completedTask : [select Phone__c,Duration__c,Call_Start_End_Time__c,Call_Id__c from Task where Call_Id__c IN :callIds.keySet()]){
        Call_log__c temp = callIds.get(completedTask.Call_id__c);
        if(temp != null){
            completedTask.Phone__c = temp.IN_Out__c;
            completedTask.Duration__c = temp.Duration__c;
            completedTask.Call_Start_End_Time__c = temp.Call_Start_End_text__c;
            tasksToUpdate.add(completedTask);
        }
    }
    if(tasksToUpdate.size() > 0){
        database.update(tasksToUpdate);
    }
}